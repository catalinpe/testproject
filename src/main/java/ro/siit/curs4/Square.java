package ro.siit.curs4;

import java.awt.*;

public class Square {

    private double squareSide;

    public Square () {

     }

    public Square (double squareSide) {

        this.squareSide = squareSide;
    }

    protected void setSide (double s) {
        /*squareSide = s;*/
       /* sau asa */
       this.squareSide = s;
    }

    public double getArea () {
        //return squareSide * squareSide;
        return Math.pow(squareSide,2); /* echivaleant cu squareside la patrat */
    }

    void printSquare () {
        System.out.println("Avem un patrat cu latura: " + squareSide + " si cu aria: " + getArea());
    }
}
