package ro.siit.curs4;

public class Draw {

    public static void main(String[] args) {
        drawFullShape(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeOutline(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        System.out.println();
        drawShapeCorners(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
    }


    public static void drawFullShape(int width, int height) {
        for (int w = 1; w < width; w++) {
            for (int h = 1; h < height; h++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    private static void drawShapeOutline(int weight, int height) {
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < weight; w++) {
                if (h == 0 || h == height - 1) { // daca suntem pe prima sau ultima line afisam numai stelute
                    System.out.print("*");
                } else { // daca nu suntem pe prima sau ultima linie
                    if (w == 0 || w == weight - 1) {  // daca suntem pe primul sau ultimul caracter de pe line
                        System.out.print("*");
                    } else { // daca nu suntem primul sau ultimul caraqcter
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }


    private static void drawShapeCorners(int weight, int height) {
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < weight; w++) {
                if (h == 0 || h == height - 1) { // daca suntem pe prima sau ultima line
                    if (w == 0 || w == weight - 1) {  // daca suntem pe primul sau ultimul caracter de pe line
                        System.out.print("*");
                    } else { // daca nu suntem primul sau ultimul caraqcter
                        System.out.print(" ");
                    }

                } else { // daca nu suntem pe prima sau ultima linie
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}


