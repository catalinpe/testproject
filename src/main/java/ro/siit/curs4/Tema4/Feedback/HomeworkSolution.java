package ro.siit.curs4.Tema4.Feedback;

public class HomeworkSolution {
    public static void main(String[] args) {
        Author auth = new Author("Alex Gatu", "alexandru.gatu@gmail.com");
        Book book = new Book("Java Basics", 2020, auth, 50);
        // Solution #1 - create a print method in Book
        book.printBook();
        // Solution #2 - do the print here
        System.out.println("Book " + book.getName() + " (" + book.getPrice() + " RON), by " + book.getAuthor().getName() + ", published in " + book.getYear());
    }
}
