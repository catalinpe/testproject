package ro.siit.curs4.Tema4.Feedback;

/*
2. Create 1 class Book as defined below
Book
name:String
year:int
author:Author
price:double
+Book(name:String, year:int, author:Author, price:double)
+getName():String
+getAuthor():Author
+getPrice():double
+getYear():int
 */

public class Book {

    private String name;
    private int year;
    private Author author;
    private double price;

    public Book() {

    }

    public Book(String name, int year, Author author, double price){
        this.name = name;
        this.year = year;
        this.author = author;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void printBook() {
        System.out.println("Book " + name + " (" + price + " RON), by " + author.getName() + ", published in " + year);
    }
}
