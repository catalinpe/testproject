package ro.siit.curs4.Tema4.Feedback;

/*
1.Create 1 class Author as defined below
Author
name:String
email:String
+Author(name:String, email:String)
+getName():String
+getEmail():String
 */

public class Author {

    private String name;
    private String email;

    public Author() {

    }

    public Author(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
