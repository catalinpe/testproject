package ro.siit.curs4.Tema4;

public class Book {

    public String title;
    public int price;
    public String author;
    public int year;

    public void BookInfo (String title, int price, String author, int year) {
        this.title = title;
        this.price = price;
        this.author = author;
        this.year = year;
    }

    //get
    public void getTitle() {
        System.out.println("Title of Book: " + title);
    }

    public void getPrice() {
        System.out.println("Price of Book: (" + price + " RON)");
    }

    public void getAuthor() {
        System.out.println("Author of Book: " + author);
    }

    public void getYear() {
        System.out.println("Published in year: " + year);
    }

    //set
    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setYear(int year) {
        this.year = year;
    }

}