package ro.siit.curs4.Tema4;

public class Author {

    //atribute clasei (variabile)
    public String name;
    public String email;

    //constructor
    public Author (String name, String email){
        this.name = name;
        this.email = email;
    }

    //metode
    //(getters)
    public String getName(){
        return name;
    }

    public String getEmail(){
        return email;
    }
    //(setters)
    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        String info = "Numele este :" + this.name + " iar adresa de email este :" + this.email;
        return info;
    }
}