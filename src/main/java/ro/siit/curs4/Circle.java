package ro.siit.curs4;

public class Circle {
    float radius;

    void setRadius (float r) {
        radius = r;
    }

    void printCircle () {
        System.out.println("Avem un cerc cu diametrul :" + radius);
    }

    float getArea () {
        final float PI = 3.141592f;
        return PI * radius * radius;
    }
}
