package ro.siit.curs4;

public class Shapes {

    public static void main(String[] args) { /* args sunt din valorile din Edit configurations */

        //Square s1 = new Square();
        //s1.setSide(Double.parseDouble(args[0]));
        Square s1 = new Square((Double.parseDouble(args[0])));
        s1.printSquare();
        System.out.println(s1.getArea());

        for (String s : args) {
            Square sq = new Square();
            sq.setSide(Double.parseDouble(s));
            sq.printSquare();
        }

        Square sq2 = new Square(100);
        System.out.println(sq2.getArea());

        Rectangle r = new Rectangle(Double.parseDouble(args[0]), Double.parseDouble(args [1]));
        System.out.println("Aria este: "+r.getArea() + " Perimetrul este: " + r.getPerimeter() + " Diagonla este: "+r.getDiagonal());


    }
}
