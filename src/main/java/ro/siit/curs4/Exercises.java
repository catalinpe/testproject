package ro.siit.curs4;

public class Exercises {

    static boolean isDivisor (int a, int d) {
        return a % d == 0;
    }

    static boolean isEven (int a) {  /*adica este numar par */
      return isDivisor(a, 2); // same as a % 2 == 0, as d will take value of 2
        //return (a % 2 == 0); /*return da automat inapoi cu valoarea true si nu se mai executa nimic*/

        //codul de mai sus este echivalent cu:
        //if (a % 2 == 0) { /*adica este numar par */
        //return true; /*return da automat inapoi cu valoarea true si nu se mai executa nimic*/
    //}
    //    return false;
    //     */
    }
//verific daca un nr este prim
    static boolean isPrime (int a) {
        for (int i = 2; i < a/2; i++) {
            if (isDivisor(a, i)) {
            //if (a % i == 0) {
                return false;
            }
        }
        return true;
    }

    static  float computeProduct (float a, float b) {
        return a * b;
    }

    static void computeSum (int a, int b) {
        System.out.println("a = "+a+" b = "+b);
        System.out.println(a+b);
    }

    public static void main (String[] args) {
        computeSum(12, 13);
        computeSum(1, 24);

        float result = computeProduct(12, 69);
        System.out.println(result);

        System.out.println(computeProduct(12,69));

        System.out.println(isEven(89)); /*nu este nr par*/
        System.out.println(isEven(90)); /*este nr par*/

        System.out.println(isPrime(51));

        Person p1 = new Person();
        Person p2 = new Person();

        p1.name = "Alex";
        p1.age = 33;
        p1.isHungry = false;

        p2.name = "Diana";
        p2.age = 18;
        p2.isHungry = true;

        System.out.println(p1.name + " si " + p2.name + " participa la cursul de automation");
        System.out.println(p1);
        System.out.println(p2);

        p1.printPerson();
        p2.printPerson();

        Circle circle1 = new Circle();
        circle1.printCircle();

        circle1.setRadius(2f);
        circle1.printCircle();
        System.out.println(circle1.radius);

        circle1.setRadius(5f);
        circle1.printCircle();
        System.out.println(circle1.radius);

        circle1.getArea();
        System.out.println("Aria cercului circle 1 este :" + circle1.getArea()); /* valoarea vine din getArea () */

        Circle circle2 = new Circle();
        circle2.setRadius(9f);
        System.out.println("Aria cercului circle 2 este :" + circle2.getArea());
    }
}
