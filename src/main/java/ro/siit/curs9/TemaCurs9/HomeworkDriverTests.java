package ro.siit.curs9.TemaCurs9;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class HomeworkDriverTests {

    WebDriver driver;

    @DataProvider (name="newaccount")
    public Iterator<Object[]> loginDp () {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //firstname, firtsname error message, lastname, lastname error message,
        //email, email error message, username, username error message,
        //password1, password1 error message, password2, password2 error message

        //CAZUL INVALID
        //fn = firstname
        //ln = lastname
        //e@a.c = email
        //abcd = username
        //1234abcd = password (password 1)
        //1234abcd = confirm password (password 2)

        //1. niciun camp completat (firstname, lastname, email, username, password1, password2)
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //2. campul firstname completat
        dp.add(new String[]{
                "fn", "", //firstname, firtsnameErrMsg
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //3. campul lastname completat
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "", //lastname, lastnameErrMsg",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //4. campul email completat
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.com", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //5. campul username completat
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //6. campul password (password 1) completat
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //7. campul confirm password (password 2) completat
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "12345678", ""
        });

        //8. campurile firstname, lastname completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //9. campurile firstname, email completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //10. campurile firstname, username completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //11. campurile firstname, password 1 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //12. campurile firstname, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //13. campurile firstname, lastname, email completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //14. campurile firstname, lastname, email, username completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //15. campurile firstname, lastname, email, password 1 completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //16. campurile firstname, lastname, email, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //17. campurile firstname, lastname, email, username, password 1 completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //18. campurile firstname, lastname, email, username, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //asta este la testul VALID 19. campurile firstname, lastname, email, username, password 1, password 2 completate

        //20. campurile firstname, email, password 1 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a,c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //21. campurile firstname, email, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //22. campurile firstname, username, password 1 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //23. campurile firstname, username, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //24. campurile firstname, username, password 1, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });

        //25. campurile firstname, password 1 completate
        dp.add(new String[]{
                "fn","",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //26. campurile firstname, password 1, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "1234abcd", ""
        });

        //27. campurile lastname, email completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //28. campurile lastname, email, username completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //29. campurile lastname, email, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //30. campurile lastname, email, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //31. campurile lastname, email, username, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //32. campurile lastname, email, username, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //33. campurile lastname, email, username, password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });

        //34. campurile lastname, username completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //35. campurile lastname, username, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //36. campurile lastname, username, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //37. campurile lastname, username, password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });

        //38. campurile lastname, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //39. campurile lastname, password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "1234abcd", ""
        });

        //40. campurile lastname, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "ln", "",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //41. campurile email, username completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "", "Please conform your password"
        });

        //42. campurile email, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //43. campurile email, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //44. campurile email, username, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //45. campurile email, username, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });


        //46. campurile email, username, password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });

        //47. campurile username, password 1 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "", "Please conform your password"
        });

        //48. campurile username, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "", "Invalid input. Please use a minimum of 8 characters",
                "1234abcd", ""
        });

        //49. campurile username, password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });


        //50. campurile password 1, password 2 completate
        dp.add(new String[]{
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter between 2 and 35 letters",
                "", "Invalid input. Please enter a valid email address",
                "", "Invalid input. Please enter between 4 and 35 letters or numbers",
                "1234abcd", "",
                "1234abcd", ""
        });


        //CAZUL VALID
        //19. campurile firstname, lastname, email, username, password 1, password 2 completate
        dp.add(new String[]{
                "fn", "",
                "ln", "",
                "e@a.c", "",
                "abcd", "",
                "1234abcd", "",
                "1234abcd", ""
        });

        return dp.iterator();
    }

    @Test (dataProvider = "newaccount")
    //valorile din paranteza vin din DataProvider
    public void negativeRegisterAccountTest (
            String firstname,
            String firstnameErrMsg,
            String lastname,
            String lastnameErrMsg,
            String email,
            String emailErrMsg,
            String usrname,
            String usrnameErrMsg,
            String password1,
            String password1ErrMsg,
            String password2,
            String password2ErrMsg) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); //this shall be moved to Before
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");

        WebElement registertabButton = driver.findElement(By.id("register-tab"));

        //navigam in tab-page-ul "Register an account"
        registertabButton.submit();
        driver.get("<a class=\"nav-link text-info active\" id=\"register-tab\" data-toggle=\"tab\" href=\"#registration_panel\" role=\"tab\" aria-controls=\"register\" aria-selected=\"true\">Register an account</a>");

        //identificam campurile in pagina
        WebElement firstnameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastnameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usrnameInput = driver.findElement(By.id("inputUsername"));
        WebElement password1Input = driver.findElement(By.id("inputPassword"));
        WebElement password2Input = driver.findElement(By.id("inputPassword2"));
        WebElement registersubmitButton = driver.findElement(By.id("register-submit"));

        //adaugam text in casute (campuri)
        firstnameInput.clear(); //goleste daca era ceva inainte
        firstnameInput.sendKeys(firstname);

        lastnameInput.clear(); //goleste daca era ceva inainte
        lastnameInput.sendKeys(lastname);

        emailInput.clear(); //goleste daca era ceva inainte
        emailInput.sendKeys(email);

        usrnameInput.clear(); //goleste daca era ceva inainte
        usrnameInput.sendKeys(usrname);

        password1Input.clear(); //goleste daca era ceva inainte
        password1Input.sendKeys(password1);

        password2Input.clear(); //goleste daca era ceva inainte
        password2Input.sendKeys(password2);

        //actionam butonul "Submit your registration"
        registersubmitButton.submit();

        WebElement err1 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(3) > div > div.invalid-feedback"));
        WebElement err2 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(4) > div > div.invalid-feedback"));
        WebElement err3 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(5) > div.input-group > div.invalid-feedback"));
        WebElement err4 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(6) > div > div.invalid-feedback"));
        WebElement err5 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(7) > div > div.invalid-feedback"));
        WebElement err6 = driver.findElement(By.cssSelector("#registration_form > div:nth-child(8) > div > div.invalid-feedback"));

        //facem validare
        //validarea testului se face pe baza assert-ului
        Assert.assertEquals(err1.getText(), firstnameErrMsg);
        Assert.assertEquals(err2.getText(), lastnameErrMsg);
        Assert.assertEquals(err3.getText(), emailErrMsg);
        Assert.assertEquals(err4.getText(), usrnameErrMsg);
        Assert.assertEquals(err5.getText(), password1ErrMsg);
        Assert.assertEquals(err6.getText(), password2ErrMsg);

        driver.close();
    }

}
