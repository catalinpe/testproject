package ro.siit.curs6;

import javax.sound.midi.Soundbank;
import java.util.*;

public class CollectionsEx {
    public static void showArrays() {
        int[] arrays = {1, 3, 4, 5};
        System.out.println("Element poz 3 :" + arrays[2]);

        //display entire array
        for (int i = 0; i < arrays.length - 1; i++) {
            System.out.println(i);
        }

        //display with forEach
        for (int el : arrays) {
            System.out.println(el);
        }

        //String [] inseamna ca este o lista
        String[] names = {"Ion", "Maria", "Vasile"};
        //String name:names inseamna pentru fiecare String din nume afiseaza elementul
        //intotdeauna listele au plural (cars, names, ...)
        for (String name : names) {
            System.out.println(name);
        }
    }

    public static void displayList(List list) {
        for (Object lst : list) {
            System.out.println(lst.toString());
        }
    }

    public static void showList() {
        List arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add("Ana");
        arrayList.add("Ana are mere");
        arrayList.add(1, 2);//adaugam un element (2) pe o anumita pozitie (1)
        displayList(arrayList);

        arrayList.remove("Ana");
        displayList(arrayList);

        List<String> names = new ArrayList<String>();
        names.add("Ion");
        names.add("Vasile");
        names.add("Vasile");
        names.add("Ana");
        System.out.println(names.toString());
        System.out.println("Size of names:" + names.size() + "-->" + names.toString());
        System.out.println(names.get(1));

        java.util.Collections.sort(names);
        System.out.println(names);

    }

    //set - list with unique elements
    public static void showSets() {
        Set<Integer> setList = new HashSet<Integer>();
        setList.add(1);
        setList.add(2);
        setList.add(3);
        setList.add(3);

        System.out.println(setList.toString());

    }

    public static void displayMap(Map hashMap) {
        List keys = new ArrayList(hashMap.keySet());
        Collections.sort(keys);
        for (Object key : keys) {
//          key -> values
            System.out.println(key + "->" + hashMap.get(key));
        }
    }


    public static void showHashMap() {
        Map<String, Integer> hashMap = new HashMap<String, Integer>();
        hashMap.put("laptop", 3);
        hashMap.put("pc", 5);
        hashMap.put("pc", 2);
        hashMap.put("mobile", 4);

        displayMap(hashMap); //ordonarea la afisare se face dupa hashcod si nu dupa ordinea pusa cu hashMap.put()
    }

    public static void main(String[] args) {
        //showArrays();
        //showList();
        //showSets();
        showHashMap();
    }
}
