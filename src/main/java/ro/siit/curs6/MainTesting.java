package ro.siit.curs6;

public class MainTesting {
    public static void main(String[] args) {
        Car car = new Truck();
        AbstractCar car2 = new Bus();

        car2.stop();

        //use Enumes
        System.out.println(Browser.CHROME);
        System.out.println(Browser.CHROME.name());

        if(Browser.CHROME.name().equals("CHROME"))
            System.out.println("CHROME browser used");
    }

}
