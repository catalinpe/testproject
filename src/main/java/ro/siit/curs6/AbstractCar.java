package ro.siit.curs6;

abstract public class AbstractCar {
    public String nrCar;

    abstract public void startEngine();
    abstract protected void stopEngine();

    public void stop(){
        System.out.println("Stop");
    }
}

