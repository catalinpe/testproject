package ro.siit.curs6.Tema6;

/*
Tema
De facut:
https://platforma.scoalainformala.ro/mod/assign/view.php?id=71481
03_Java_Building_Homework.pdf

Structura de date de intrare:

    		            |office	    |Toilet	    |Kitchen	|Conference	    |TOTAL
    		            |-----------|-----------|-----------|---------------|-----
Building	1st-floor	1	        2	        1	        3	            7
	        2nd-floor	2	        3	        2	        4	            11
	        3rd-floor	3	        4	        3	        5	            15
	                    |-----------|-----------|-----------|---------------|-----
	            TOTAL	6	        9	        6	        12	            33

	                min	1	        2	        1	        3
	                max	3	        4	        3	        5
*/

import ro.siit.curs6.Browser;

import java.util.*;

import java.text.DecimalFormat;

public class BuildingStructure {
    public static void main(String[] args) {

        int floor;//Define # of FLOORS

        int rooms;//Define # of ROOMS
        int totalRooms;//Gives the total # of ROOMS

        int office;//Define # of OFFICE rooms
        int totalOffices;//Gives the total # of OFFICE rooms
        int minoffice = 1;
        int maxoffice = 3;

        int toilets;//Define # of TOILET rooms
        int totalToilets;//Gives the total amount of TOILET rooms
        int mintoilets = 2;
        int maxtoilets = 4;

        int kitchen;//Define # of KITCHEN rooms
        int totalKitchen;//Gives the total # of KITCHEN rooms
        int minkitchen = 1;
        int maxkitchen = 3;

        int conference;//Define # of CONFERENCE rooms
        int totalConference;//Gives the total # of CONFERENCE rooms
        int minconference = 3;
        int maxconference = 5;

        /*This is for the input of information
         *for this was imported java.util.Scanner;*/
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Please enter the number of Floors in the Building: ");
        floor = keyboard.nextInt(); //Entry the amount of FLOORS

        while (floor < 3) {
            System.out.print("The number of --Floors-- cannot be less than 3, please re-enter: ");
            floor = keyboard.nextInt(); //Error message pertaining to # of FLOORS
        }

        //Totals variable initialization
        totalRooms = 0; //Always remember to have a value of 0
        totalOffices = 0; //Always remember to have a value of 0
        totalToilets = 0; //Always remember to have a value of 0
        totalKitchen = 0; //Always remember to have a value of 0
        totalConference = 0; //Always remember to have a value of 0

        for (int i = 1; i <= floor; i++) {

            //ROOMS
            System.out.print("Please enter the number of Rooms in Floor " + i + ": ");
            rooms = keyboard.nextInt(); //Entry the amount of ROOMS in a FLOOR

            while (rooms < 7)//while statement

//----------------------------------------------------------------------------------------------
                /*problema 1:
                 *cum sa fac aceasta bucla pentru fiecare Floor
                 *(la floor 1 sunt 7 rooms, la floor 2 sunt 11 rooms, la floor 3 sunt 15 rooms
                 *acum la executie imi apare de fiecare data validarea pentru valoarea 7
                 */ {
                System.out.print("Number of rooms cannot be less than 7, please re-enter: ");
                rooms = keyboard.nextInt(); //Error message pertaining to the amount of ROOMS
            }

            totalRooms += rooms;

            //OFFICES ROOMS
            System.out.print("Please enter the number of --Office-- rooms in Floor " + i + ": ");
            office = keyboard.nextInt(); //Entry the amount of OFFICE rooms

//--------------------------------------------------------------------------------------------
            /*problema 2:
             *cum sa fac aceasta bucla pentru fiecare Office sa fac o validare mai corecta
             *cu min si max definite pentru fiecare tip de room
             */

            while (office > rooms || office < 0 || office < minoffice || office > maxoffice) //While statement

            {
                System.out.print("The number of --Office-- rooms must be between " + minoffice + " and " + maxoffice + ". Please re-enter number of --Office--: ");
                office = keyboard.nextInt(); //Error message pertaining to the amount of OFFICE rooms
            }

            totalOffices += office;

            //TOILET ROOMS
            System.out.print("Please enter the number of --Toilet-- rooms in floor " + i + ": ");
            toilets = keyboard.nextInt(); //Entry of the amount of TOILETS rooms

            while (toilets > rooms || toilets < 0 || toilets < mintoilets || toilets > maxtoilets) //While statement
            {
                System.out.print("The number of --Toilet-- rooms must be between " + mintoilets + " and " + maxtoilets + ". Please re-enter number of --Toilet--");
                toilets = keyboard.nextInt(); //Error message pertaining to amount of TOILETS rooms
            }

            totalToilets += toilets;

            //KITCHEN ROOMS
            System.out.print("Please enter the number of --Kitchen-- rooms in floor " + i + ": ");
            kitchen = keyboard.nextInt(); //Entry the amount of KITCHEN rooms

            while (kitchen > rooms || kitchen < 0 || kitchen < minkitchen || kitchen > maxkitchen) //While statement
            {
                System.out.print("The number of --Kitchen-- rooms must be between " + minkitchen + " and " + maxkitchen + ". Please re-enter number of --Kitchen--");
                kitchen = keyboard.nextInt(); //Error message pertaining to amount of KITCHEN rooms
            }

            totalKitchen += kitchen;

            //CONFERENCE ROOMS
            System.out.print("Please enter the number of --Conference-- rooms in floor " + i + ": ");
            conference = keyboard.nextInt(); //Entry the amount of CONFERENCE rooms

            while (conference > rooms || conference < 0 || conference < minconference || conference > maxconference) //While statement
            {
                System.out.print("The number of --Conference-- rooms must be between " + minconference + " and " + maxconference + ". Please re-enter the number of --Conference--");
                conference = keyboard.nextInt(); //Error message pertaining to amount of CONFERENCE rooms
            }

            totalConference += conference;

//-------------------------------------------------------------------------------------
            /*problema 3:
             *cum sa fac aceasta afisare pentru fiecare Floor?
             *acum la fiecare floor aduna la total si rooms de la floor-ul anterior
             */

            System.out.println("At the FLOOR " + i + " are " + totalRooms + " ROOMS: " + office + " 'Office rooms'," + toilets + " 'Toilets rooms'," + kitchen + " 'Kitchen rooms' and ," + conference + " 'Conference rooms.");
        }

//-------------------------------------------------------------------------------------
        /*problema 4:
         *cum sa fac aceasta afisare pentru fiecare Floor?
         *aici inainte de totaluri
         */

        System.out.println("The total number of 'Office rooms' in the Building is " + totalOffices);
        System.out.println("The total number of 'Toilet rooms' in the Building is " + totalToilets);
        System.out.println("The total number of 'Kitchen rooms' in the Building is " + totalKitchen);
        System.out.println("The total number of 'Conference rooms' in the Building is " + totalConference);
        System.out.println("The total number of 'Rooms' in the Building is " + totalRooms);

//-------------------------------------------------------------------------------------
        /*problema 5:
         *cum sa fac aici aceasta afisare de camere pentru fiecare Floor chemat cu Enums?
         */

        //Enums
        //System.out.println("At the Floor "+Floor.FLOOR1+" the rooms are "+office+" Office, "+toilets+" Toilets, "+kitchen+" Kitchen, "+conference+" Conference rooms");
        //System.out.println("At the Floor "+Floor.FLOOR2+" the rooms are "+office+" Office, "+toilets+" Toilets, "+kitchen+" Kitchen, "+conference+" Conference rooms");
        //System.out.println("At the Floor "+Floor.FLOOR3+" the rooms are "+office+" Office, "+toilets+" Toilets, "+kitchen+" Kitchen, "+conference+" Conference rooms");
    }

    /*problema 6:
    *nu m-am priceput cum sa folosesc
    * - To override toString()
    * - To use inheritance
    * - To use lists
    * - To use maps
    * - To use enums
    * vreau mult sa stiu cum se folosesc
     */
}
