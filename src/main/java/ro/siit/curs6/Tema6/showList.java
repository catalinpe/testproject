package ro.siit.curs6.Tema6;

import java.util.ArrayList;
import java.util.List;

public class showList {
    public static void showList() {
        List<String> names = new ArrayList<String>();
        names.add("Floor1");
        names.add("Floor2");
        names.add("Floor3");
        System.out.println(names.toString());
    }

    public static void main(String[] args) {
        showList();
    }
}