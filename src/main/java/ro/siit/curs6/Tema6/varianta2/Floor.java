package ro.siit.curs6.Tema6.varianta2;

public class Floor extends Building{
    //private int numClassRooms;
    //private String gradeLevel;

    private int numRooms;
    private String typeRooms;

    public Floor(int floors, int numRooms, String typeRooms) {
        super(floors);
        this.numRooms = numRooms;
        this.typeRooms = typeRooms;
    }

    //numRooms
    public int getNumRooms() {
        return numRooms;
    }

    public void setNumRooms(int numRooms) {
        this.numRooms = numRooms;
    }

    //typeRooms
    public String getTypeRooms() {
        return typeRooms;
    }

    public void setTypeRooms(String typeRooms) {
        this.typeRooms = typeRooms;
    }

    @Override
    public String toString() {
        return "Floor:" + super.toString() + " with " + numRooms + " Rooms"
                + " of different types: " + typeRooms ;
    }
}
