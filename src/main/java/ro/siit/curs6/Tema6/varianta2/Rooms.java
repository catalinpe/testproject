package ro.siit.curs6.Tema6.varianta2;

/*

    		            |office	    |Toilet	    |Kitchen	|Conference	    |TOTAL
    		            |-----------|-----------|-----------|---------------|-----
Building	1st-floor	1	        2	        1	        3	            7
	        2nd-floor	2	        3	        2	        4	            11
	        3rd-floor	3	        4	        3	        5	            15
	                    |-----------|-----------|-----------|---------------|-----
	            TOTAL	6	        9	        6	        12	            33

	                min	1	        2	        1	        3
	                max	3	        4	        3	        5
 */

public class Rooms extends Building {
    private int numOfficeRooms;
    private int numToiletRooms;
    private int numKitchenRooms;
    private int numConferenceRooms;

    public Rooms(int floors, int numOfficeRooms, int numToiletRooms, int numKitchenRooms, int numConferenceRooms) {
        super(floors);
        this.numOfficeRooms = numOfficeRooms;
        this.numToiletRooms = numToiletRooms;
        this.numKitchenRooms = numKitchenRooms;
        this.numConferenceRooms = numConferenceRooms;
    }

    //numOfficeRooms
    public int getNumOfficeRooms() {
        return numOfficeRooms;
    }

    public void setNumOfficeRooms(int numOfficeRooms) {
        this.numOfficeRooms = numOfficeRooms;
    }

    //numToiletRooms
    public int getNumToiletRooms() {
        return numToiletRooms;
    }

    public void setNumToiletRooms(int numToiletRooms) {
        this.numToiletRooms = numToiletRooms;
    }

    //numKitchenRooms
    public int getNumKitchenRooms() {
        return numKitchenRooms;
    }

    public void setNumKitchenRooms(int numKitchenRooms) {
        this.numKitchenRooms = numKitchenRooms;
    }

    //numConferenceRooms
    public int getNumConferenceRooms() {
        return numConferenceRooms;
    }

    public void setNumConferenceRooms(int numConferenceRooms) {
        this.numConferenceRooms = numConferenceRooms;
    }

    @Override
    public String toString() {
        return "Rooms:" + super.toString()
                + ": Office Rooms " + numOfficeRooms
                + ", Toilet Rooms " + numToiletRooms
                + ", Kitchen Rooms " + numKitchenRooms
                + " and Conference Rooms " + numConferenceRooms + "]";
    }

}
