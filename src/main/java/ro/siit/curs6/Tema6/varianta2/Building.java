package ro.siit.curs6.Tema6.varianta2;

import java.util.*;

//Building class
public class Building {
    //properties of building
    public int floors;

    // 1 argument constructor
    public Building(int floors) {
        this.floors = floors;
    }

    //floors
    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public static void displayFloors(Map hashFloors) {
        List keys = new ArrayList(hashFloors.keySet());
        Collections.sort(keys);
        for (Object key : keys) {
            System.out.println(key + "->" + hashFloors.get(key));
        }
    }

    public static void showFloors() {
        Map<String, Integer> hashFloor = new HashMap<String, Integer>();
        hashFloor.put("floor = 1 with number of rooms = ", 7);
        hashFloor.put("floor = 2 with number of rooms = ", 11);
        hashFloor.put("floor = 3 with number of rooms = ", 15);

        displayFloors(hashFloor);
    }

    // toString method
    @Override
    public String toString() {
        return "[floors = " + floors + "]";
    }
}
