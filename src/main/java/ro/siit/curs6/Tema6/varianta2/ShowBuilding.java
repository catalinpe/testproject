package ro.siit.curs6.Tema6.varianta2;

public class ShowBuilding {
    public static void main(String[] args) {
        //Problema 1
        // Nu stiu cum sa rulez metoda showFloors() din clasa Building

        Building build = new Building(3);
        System.out.println("The Building has " + build + ", detailed side by side:");
        System.out.println();

        //Problema 2
        //Nu stiu cum sa utilizez TypeRooms enum class

        //Get all enums
        /*
        for(TypeRooms typeRooms : TypeRooms.values()) {
            System.out.println(typeRooms.name() + typeRooms.getTypes());
        }
        */

        Building floor1 = new Floor(1, 7, "Office, Toilet, Kitchen and Conference");

        Building floor1Rooms = new Rooms(1, 1, 2, 1, 3);

        System.out.println(floor1);
        System.out.println(floor1Rooms);
        System.out.println();

        Building floor2 = new Floor(2, 11, "Office, Toilet, Kitchen and Conference");
        Building floor2Rooms = new Rooms(2, 2, 3, 2, 4);
        System.out.println(floor2);
        System.out.println(floor2Rooms);
        System.out.println();

        Building floor3 = new Floor(3, 15, "Office, Toilet, Kitchen and Conference");
        Building floor3Rooms = new Rooms(3, 3, 4, 3, 5);
        System.out.println(floor3);
        System.out.println(floor3Rooms);

    }
}
