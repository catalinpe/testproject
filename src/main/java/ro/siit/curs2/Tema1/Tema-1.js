
function validateNotEmpty(element){
	if (element=='' || element==null){
		return false;
	} else 
	return true;
}

function showFullInformation(){
	var firstName = document.getElementById("firstName").value;
	var lastName = document.getElementById("lastName").value;
	var eMail = document.getElementById ("eMail").value;
	var userName = document.getElementById("userName").value;	
	var valPassword = document.getElementById("valPassword").value;	
	var confPassword = document.getElementById("confPassword").value;
	var valGender = document.getElementById("valGender").value;
	var profPhoto = document.getElementById("profPhoto").value;
	var aboutYou = document.getElementById("aboutYou").value;
	var subscribeNews = document.getElementById("subscribeNews").value;
	var fullInformationTag = document.getElementById("fullInformation");
	var message='';
	
	//manipulate DOM
	if(validateNotEmpty(firstName)){
		if(validateNotEmpty(lastName)){
			if(validateNotEmpty(eMail)){
				if(validateNotEmpty(userName)){
					if(validateNotEmpty(valPassword)){
						if(validateNotEmpty(confPassword)){
							if(validateNotEmpty(valGender)){
								if(validateNotEmpty(profPhoto)){
									if(validateNotEmpty(aboutYou)){
										if(validateNotEmpty(subscribeNews)){
											message = "Full Information: |First Name: "+firstName+" |Last Name: "+lastName+" |E-eMail: "+eMail+" |Username: "+userName+" |Password: "+valPassword+" |Confirm-password: "+confPassword+" |Gender: "+valGender+" |Profile Photo: "+profPhoto+" |About yourself: "+aboutYou+" |Subscribe to newsletter: "+subscribeNews; 
										} else {message = "Subscribe-to-newsletter should not be empty";}
									} else {message = "About-yourself should not be empty";} 
								} else {message = "Profile-photo should not be empty";}
							} else {message = "Gender should not be empty";}
						}else {message = "Confirm password should not be empty";}
					} else {message = "Password should not be empty";}
				} else {message = "Username should not be empty";}				
			} else {message = "E-mail should not be empty";}
		} else {message = "Last name should not be empty";}
	} else {message = "First name should not be empty";}
	displayValues(fullInformationTag,message);
}

/* NU STIU CE FACE FUNCTIA ASTA */
function displayValues(element, message){
	element.innerHTML=message;
}

/* NU STIU CE FACE FUNCTIA ASTA */
function showAlert2()
{
	window.alert("This is a JavaScript alert from outter file!");
}
*/

/* JavaScript code for validation
	
JavaScript function which is called on onSubmit
	
This function calls all other functions used for validation. */

function formValidation()
{
	var ufname = document.registration.firstname;
	var ulname = document.registration.lastname;
	var uemail = document.registration.email;
	var uname = document.registration.username;
	var uvpass = document.registration.vpass;
	var ucpass = document.registration.cpass;
	var umsex = document.registration.msex;
	var ufsex = document.registration.fsex; 
	var upphoto = = document.registration.pphoto
	var uabout = document.registration.about
	
	if(firstname_validation(ufname,2,40)) {
		if(lastname_validation(ulname,2,40)) {
			if(email_validation(uemail,2,40)) {
				if(username_validation(uname,2,40)) {
					if(valpassword_validation(uvpass,2,40)) {
						if(conpassword_validation(ucpass,2,40)) {
							if(validsex(umsex,ufsex)) {
								if(about_validation(uabout,2,40)) {
								}
							} 
						}
					} 
				}
			}
		}
	}
	return false;
}


/* if(firstname_validation(ufname,2,40)) { */
function firstname_validation(ufname,mx,my)
{
	var ufname_len = ufname.value.length;
	if (ufname_len == 0 || ufname_len >= my || ufname_len < mx)
	{
		alert("First name should not be empty / length be between "+mx+" to "+my);
		ufname.focus();
		return false;
	}
	return true;
}

/* if(lastname_validation(ulname,2,40)) { */
function lastname_validation(ulname,mx,my)
{
	var ulname_len = ulname.value.length;
	if (ulname_len == 0 || ulname_len >= my || ulname_len < mx)
	{
		alert("Last name should not be empty / length be between "+mx+" to "+my);
		ulname.focus();
		return false;
	}
	return true;
}

/* if(email_validation(uemail,2,40)) { */
function email_validation(uemail,mx,my)
{
	var uemail_len = uemail.value.length;
	if (uemail_len == 0 || uemail_len >= my || uemail_len < mx)
	{
		alert("Email should not be empty / length be between "+mx+" to "+my);
		uemail.focus();
		return false;
	}
	return true;
}

/* if(username_validation(uname,2,40)) { */
function username_validation(uname,mx,my)
{
	var uname_len = uname.value.length;
	if (uname_len == 0 || uname_len >= my || uname_len < mx)
	{
		alert("Username should not be empty / length be between "+mx+" to "+my);
		uname.focus();
		return false;
	}
	return true;
}

/* if(password_validation(uvpass,2,40)) { */
function uvpass_validation(uvpass,mx,my)
{
	var uvpass_len = uvpass.value.length;
	if (uvpass_len == 0 || uvpass_len >= my || uvpass_len < mx)
	{
		alert("Password should not be empty / length be between "+mx+" to "+my);
		uvpass.focus();
		return false;
	}
	return true;
}

/* if(password_validation(ucpass,2,40)) { */
function ucpass_validation(ucpass,mx,my)
{
	var ucpass_len = ucpass.value.length;
	if (ucpass_len == 0 || ucpass_len >= my || ucpass_len < mx)
	{
		alert("Confirmation password should not be empty / length be between "+mx+" to "+my);
		ucpass.focus();
		return false;
	}
	return true;
}


/* if(aboutyourself_validation(ucpassbout2,40)) { */
function uabout_validation(uabout,mx,my)
{
	var uabout_len = uabout.value.length;
	if (uabout_len == 0 || uabout_len >= my || uabout_len < mx)
	{
		alert("About yourself should not be empty / length be between "+mx+" to "+my);
		uabout.focus();
		return false;
	}
	return true;
}

