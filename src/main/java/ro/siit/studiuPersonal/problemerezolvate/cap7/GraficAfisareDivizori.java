package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine patru componente grafice:
    -JTextField pentru introducerea unui număr întreg,
    -JTextField pentru afişare și
    -Două componente JButton.

Atunci când este apăsat primul buton, se vor afişa toţi divizorii numărului introdus.
Când se apasă cel de-al doilea buton, se iese din program.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GraficAfisareDivizori
{
    public static void main(String args[])
    {
        Fereastra1 f=new Fereastra1();
        f.setTitle("Calcul divizori");
        f.setVisible(true);
    }
}
class Fereastra1 extends JFrame
{
    private JTextField jtfDate, jtfAfisare;
    private JButton jbCalcul, jbExit;
    //constructor
    public Fereastra1()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
//construieste componente:
        JLabel jl1=new JLabel("Numar=");
        jtfDate=new JTextField(5);//pt. introducere numar

        JPanel jp1=new JPanel();
        jp1.add(jl1); jp1.add(jtfDate);

        JLabel jl2=new JLabel("Divizorii: ");
        jtfAfisare=new JTextField(40);
        jtfAfisare.setEditable(false) ;

        JPanel jp2=new JPanel();
        jp2.add(jl2); jp2.add(jtfAfisare);
        jbCalcul=new JButton("Calcul");
        jbExit=new JButton("Exit");

        JPanel jp3=new JPanel();
        jp3.add(jbCalcul); jp3.add(jbExit);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(3,1));
        jp.add(jp1); jp.add(jp2); jp.add(jp3);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbCalcul.addActionListener(bL);
        jbExit.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jp,"South");
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class

    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            Object sursa=ev.getSource();
            if(sursa==jbExit)System.exit(0);
            if(sursa==jbCalcul){
                int nr=Integer.parseInt(jtfDate.getText());
                String rezultat="1, ";//primul divizor
                for(int i=2;i<=nr/2;i++)
                    if(nr%i==0)rezultat=rezultat+i+", ";
//Ultimul divizor este chiar nr:
                rezultat=rezultat+nr+".";
                jtfAfisare.setText(rezultat);
            }
        }
    }//end inner class
}//end class Fereastra1