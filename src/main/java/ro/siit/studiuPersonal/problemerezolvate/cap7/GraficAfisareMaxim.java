package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine patru componente grafice:
    -JTextField pentru introducerea elementelor unui vector de numere intregi (elementele sunt separate prin spaţii),
    -JTextField pentru afişarea maximului din vector şi
    -Două componente JButton.

Atunci când este apăsat primul buton, se va afişa maximul din vectorul introdus.
Când se apasă cel de-al doilea buton, se iese din program.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
class GraficAfisareMaxim
{
    public static void main(String args[])
    {
        Fereastra2 f=new Fereastra2();
        f.setTitle("Calcul maxim");
        f.setVisible(true);
    }
}
class Fereastra2 extends JFrame
{
    private JTextField jtfDate, jtfAfisare;
    private JButton jbCalcul, jbExit;
    //constructor
    public Fereastra2()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
//construieste componente:
        JLabel jl1=new JLabel("Vector=");
        jtfDate=new JTextField(40);//pt. introducere numar

        JPanel jp1=new JPanel();
        jp1.add(jl1); jp1.add(jtfDate);

        JLabel jl2=new JLabel("Maxim: ");
        jtfAfisare=new JTextField(5);
        jtfAfisare.setEditable(false) ;

        JPanel jp2=new JPanel();
        jp2.add(jl2); jp2.add(jtfAfisare);
        jbCalcul=new JButton("Calcul");
        jbExit=new JButton("Exit");

        JPanel jp3=new JPanel();
        jp3.add(jbCalcul); jp3.add(jbExit);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(3,1));
        jp.add(jp1); jp.add(jp2); jp.add(jp3);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbCalcul.addActionListener(bL);
        jbExit.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jp,"South");
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class
    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            Object sursa=ev.getSource();
            if(sursa==jbExit)System.exit(0);
            if(sursa==jbCalcul){
//preluarea elementelor vectorului, din JTextField:
                String s=jtfDate.getText();
                StringTokenizer tk=new StringTokenizer(s);
                int N=tk.countTokens();//nr. de elemente din vector
//initializam maximul cu primul numar :
                int max=Integer.parseInt(tk.nextToken());

//Comparam cu restul numerelor:
                for(int i=1;i<N;i++){
                    int nrCrt=Integer.parseInt(tk.nextToken());
                    if(nrCrt>max)max=nrCrt;
                }
//Afisare maxim:
                jtfAfisare.setText(""+max);
            }
        }
    }//end inner class
}//end class Fereastra2