package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine trei componente grafice:
    -JTextArea pentru introducerea pe mai multe linii a unor numere întregi (în fiecare linie sunt mai multe numere
separate prin spaţii),
    -JTextField pentru afişare şi
    -JButton pentru comanda efectuării calculelor.

Atunci când se apasă butonul, în JTextField se va afişa suma tuturor numerelor din JTextArea.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

class AfisareSuma
{
    public static void main(String args[])
    {
        Fereastra3 f=new Fereastra3();
        f.setTitle("Suma numerelor");
        f.setVisible(true);
    }
}
class Fereastra3 extends JFrame
{
    private JTextArea jta;
    private JTextField jtfAfisare;
    private JButton jbCalcul;
    //constructor
    public Fereastra3()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
        jta=new JTextArea();
        jtfAfisare=new JTextField(5);
        jtfAfisare.setEditable(false) ;

        JPanel jp1=new JPanel();
        jp1.add(jtfAfisare);
        jbCalcul=new JButton("Calcul");

        JPanel jp2=new JPanel();
        jp2.add(jbCalcul);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(2,1));
        jp.add(jp1); jp.add(jp2);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbCalcul.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jta,"Center");
        c.add(jp,"South");
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class
    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            String s=jta.getText();
//Extrag numerele :
            StringTokenizer tk=new StringTokenizer(s);
            int N=tk.countTokens();
            int suma=0;
            for(int i=0;i<N;i++)
                suma=suma+Integer.parseInt(tk.nextToken());
//Afisare suma:
            jtfAfisare.setText(""+suma);
        }
    }
}//end class Fereastra3