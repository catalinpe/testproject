package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine următoarele componente grafice:
    -JTextField pentru afişarea textului: Afisare cu diferite marimi de fonturi !,
    -Trei butoane radio JRadioButton, pentru selecţia mărimii fontului cu care se scrie textul
din JTextField ( font mic, font mediu, font mare ), şi
    -Un buton JButton pentru ieşirea din program.

Atunci
-când este activat primul buton radio, textul se va afişa în JTextField folosind font de mărime 12,
-când este activat al doilea buton radio, textul se va afişa cu font de mărime 18,
-când este activat al treilea, se va folosi font de mărime 24.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GraficButoaneRadio
{
    public static void main(String args[])
    {
        Fereastra5 f=new Fereastra5();
        f.setTitle("Butoane radio");
        f.setVisible(true);
    }
}
class Fereastra5 extends JFrame
{
    private JTextField jtfAfisare;
    private JButton jbExit;
    private JRadioButton radio1,radio2,radio3;
    //constructor
    public Fereastra5()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
//construieste componente:
        jtfAfisare=new JTextField("Afisare cu diferite marimi de fonturi !");
        jtfAfisare.setEditable(false) ;
        radio1= new JRadioButton("font mic");
        radio1.setSelected(true);
        radio2= new JRadioButton("font mediu");
        radio2.setSelected(false);
        radio3= new JRadioButton("font mare");
        radio3.setSelected(false);
        ButtonGroup radioGrup= new ButtonGroup();
        radioGrup.add(radio1);
        radioGrup.add(radio2);
        radioGrup.add(radio3);
        JPanel jpRadio= new JPanel();
//in JPanel nu este permisa adaugarea de ButtonGroup !
        jpRadio.add(radio1);
        jpRadio.add(radio2);
        jpRadio.add(radio3);
        jbExit=new JButton("Exit");
        JPanel jpExit=new JPanel();
        jpExit.add(jbExit);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(2,1));
        jp.add(jpRadio); jp.add(jpExit);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbExit.addActionListener(bL);
        radio1.addActionListener(bL);
        radio2.addActionListener(bL);
        radio3.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jtfAfisare,"Center");
        c.add(jp,"South");
//Scriem textul in jtfAfisare cu font mic (marimea 12):
        //jtfAfisare.setFont(new Font("Monospaced",Font.ITALIC,12));
        jtfAfisare.setFont(new Font("Arial Black",Font.ITALIC,12));
        jtfAfisare.repaint();
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class
    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            Object sursa=ev.getSource();
            if(sursa==jbExit)System.exit(0);
            else{
                //String tipFont="MonoSpaced";
                String tipFont="Arial Black";
                int stilFont=Font.ITALIC;
                int marimeFont=12;
                if(radio2.isSelected())marimeFont=18;
                else if(radio3.isSelected())marimeFont=24;
                jtfAfisare.setFont(new Font(tipFont,stilFont,marimeFont));
                jtfAfisare.repaint();
            }
        }
    }
}//end class Fereastra5