package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine trei componente grafice:
    -JTextArea pentru introducerea unui text,
    -JTextField pentru afişare şi
    -JButton pentru comanda efectuării calculelor.

Atunci când se apasă butonul, în JTextField se va afişa numarul de caractere ’a’ din textul introdus în JTextArea.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Calcul
{
    public static void main(String args[])
    {
        Fereastra4 f=new Fereastra4();
        f.setTitle("Calcul nr. caractere a");
        f.setVisible(true);
    }
}

class Fereastra4 extends JFrame
{
    private JTextArea jta;
    private JTextField jtfAfisare;
    private JButton jbCalcul;
    //constructor
    public Fereastra4()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
        jta=new JTextArea();
        jtfAfisare=new JTextField(5);
        jtfAfisare.setEditable(false) ;

        JPanel jp1=new JPanel();
        jp1.add(jtfAfisare);
        jbCalcul=new JButton("Calcul");

        JPanel jp2=new JPanel();
        jp2.add(jbCalcul);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(2,1));
        jp.add(jp1); jp.add(jp2);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbCalcul.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jta,"Center");
        c.add(jp,"South");
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class
    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            String s=jta.getText();
            int contor=0;
            for(int i=0;i<s.length();i++)
                if(s.charAt(i)=='a')contor++;
            jtfAfisare.setText(""+contor);
        }
    }
}//end class Fereastra3