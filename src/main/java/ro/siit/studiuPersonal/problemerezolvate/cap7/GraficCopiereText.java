package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*
Să se scrie o aplicaţie în care se afişează o fereastră ce conţine trei componente grafice:
    -JTextField pentru introducerea unui text,
    -JTextField pentru afişare şi
    -Un buton JButton pentru ieşirea din program.

Atunci când se apasă tasta ENTER în primul JTextField, textul introdus acolo va fi copiat în al doilea JTextField.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GraficCopiereText
{
    public static void main(String args[])
    {
        Fereastra6 f=new Fereastra6();
        f.setTitle("Copiere text");
        f.setVisible(true);
    }
}
class Fereastra6 extends JFrame
{
    private JTextField jtfDate,jtfAfisare;
    private JButton jbExit;
    //constructor
    public Fereastra6()
    {
        setSize(600,400);
        addWindowListener(new FereastraListener());
//construieste componente:
        jtfDate=new JTextField(40);
        jtfDate.setEditable(true);
        jtfDate.addKeyListener(new ClasaTasteListener());

        JPanel jp1=new JPanel();
        jp1.add(jtfDate);
        jtfAfisare=new JTextField(40);
        jtfAfisare.setEditable(false);

        JPanel jp2=new JPanel();
        jp2.add(jtfAfisare);
        jbExit=new JButton("Exit");

        JPanel jp3=new JPanel();
        jp3.add(jbExit);
//Container intermediar:
        JPanel jp=new JPanel();
        jp.setLayout(new GridLayout(3,1));
        jp.add(jp1); jp.add(jp2); jp.add(jp3);
        ClasaButoaneListener bL=new ClasaButoaneListener();
        jbExit.addActionListener(bL);
//Containerul final:
        Container c=this.getContentPane();
        c.add(jp,"South");
    }
    //inner class:
    private class FereastraListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent ev)
        {
            System.exit(0);
        }
    }//end inner class
    //inner class:
    private class ClasaButoaneListener implements ActionListener
    {
        public void actionPerformed(ActionEvent ev)
        {
            System.exit(0);
        }
    }
    private class ClasaTasteListener extends KeyAdapter
    {
        public void keyPressed(KeyEvent evt)
        {
// S-a tastat ENTER ?
            if (evt.getKeyChar() == '\n') {
                jtfAfisare.setText(jtfDate.getText());
            }
        }
    }
}//end class Fereastra6