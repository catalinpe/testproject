package ro.siit.studiuPersonal.problemerezolvate.cap7;

/*Să se scrie o aplicaţie ce conţine patru componente grafice:
        -JTextField pentru afişare şi
        -trei butoane JButton, notate cu “unu”, ”doi” şi ”trei”.

Cele trei butoane vor fi aşezate în linie.
La apăsarea unui buton se va afişa în componenta JTextField numele butonului apăsat.
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ScrieNumeButon {
    public static void main(String args[]) {
        Fereastra f = new Fereastra();
        f.setTitle("Test de butoane");
        f.setVisible(true);
    }
}

class Fereastra extends JFrame {
    private JTextField jtf;
    private JButton jb1, jb2, jb3;

    //constructor
    public Fereastra() {
        setSize(300, 300);
        addWindowListener(new FereastraListener());
//construieste componente:
        jtf = new JTextField();
//doar pt. afisare:
        jtf.setEditable(false);
//Construim cele trei butoane:
        jb1 = new JButton("buton unu");
        jb2 = new JButton("buton doi");
        jb3 = new JButton("buton trei");
//Le plasez intr-un container intermediar JPanel:
        JPanel jp = new JPanel();
        jp.add(jb1);
        jp.add(jb2);
        jp.add(jb3);
//Atasam fiecarui buton, obiectul de ascultare, pentru a putea fi tratate evenimentele de apasare a acestor butoane:
        ClasaButoaneListener listener = new ClasaButoaneListener();
        jb1.addActionListener(listener);
        jb2.addActionListener(listener);
        jb3.addActionListener(listener);
//adaugam componentele in containerul final, ce se extrage din fereasta:
        Container containerFinal = this.getContentPane();
        containerFinal.add(jtf, "Center"); //daca lipseste, nu deseneaza nimic in centru
        containerFinal.add(jp, "South");//le aseaza in partea de jos a ferestrei
    }

    //clasa interioara:
    private class FereastraListener extends WindowAdapter {
        public void windowClosing(WindowEvent ev) {
            System.exit(0);
        }
    }

    private class ClasaButoaneListener implements ActionListener {
        //Interfata ActionListener are o singura metoda:
        public void actionPerformed(ActionEvent ev) {
            Object sursa = ev.getSource();
            if (sursa == jb1) jtf.setText("a fost apasat butonul unu");
            else if (sursa == jb2) jtf.setText("a fost apasat butonul doi");
            else if (sursa == jb3) jtf.setText("a fost apasat butonul trei");
        }
    }
}