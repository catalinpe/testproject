package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*11. Se citesc trei numere întregi a, b şi c, de la tastatură. Să se
        calculeze
        -maximul dintre a şi b (se va folosi metoda definită separat maxim(), ce are doi parametrii) şi
        -maximul dintre a, b şi c (se va folosi metoda definită separat maxim(), ce are trei parametrii).
        Această aplicaţie ilustrează ===polimorfismul parametric=== în Java (posibilitatea ca
        în aceeaşi clasă să existe două sau mai multe metode cu acelaşi nume,
        dar cu liste de parametrii diferiţi).
*/

import javax.swing.*;

class CalculMaxim
{
    public static void main(String args[])
    {
        int a=Integer.parseInt(JOptionPane.showInputDialog("a="));
        int b=Integer.parseInt(JOptionPane.showInputDialog("b="));
        int c=Integer.parseInt(JOptionPane.showInputDialog("c="));
        int max2=maxim(a,b);
        System.out.println(max2);
        int max3=maxim(a,b,c);
        System.out.println(max3);
    }
    private static int maxim(int a,int b){
        if(a>=b)return a;
        else return b;
    }
    private static int maxim(int a,int b,int c){
        int max=a;
        if(b>max)max=b;
        if(c>max)max=c;
        return max;
    }
}