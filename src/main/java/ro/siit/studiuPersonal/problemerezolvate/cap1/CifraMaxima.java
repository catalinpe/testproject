package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*10. Se citeşte un număr natural de la tastatură. Să se afişeze care
        este cifra maximă din acest număr.
*/

import javax.swing.*;

class CifraMaxima {
    public static void main(String args[]) {
        int a = Integer.parseInt(JOptionPane.showInputDialog("a="));
        int max = 0;
        for (; ; ) { //Semantically, for (; ;) si while (true) they're completely equivalent.
            int cifraCrt = a % 10;
            if (cifraCrt > max) max = cifraCrt;
            a = a / 10;//reduc numarul:
            if (a == 0) break;
        }
        System.out.println(max);
    }
}