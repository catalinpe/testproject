package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*14. Se citeşte un număr natural N. Să se afişeze dacă acest număr
        este termen în şirul lui Fibonacci.
*/

import javax.swing.*;

class EsteFibonacci
{
    public static void main(String args[])
    {
        int x=Integer.parseInt(JOptionPane.showInputDialog("x="));
        boolean este=false;
        int n=0;
        for(;;){
            int termen=fib(n);
            if(x==termen){
                este=true;
                break;
            }
            if(x<termen)break; //deci, nu este
            // trecem la urmatorul termen Fibonacci:
            n++;
        }
        if(este)System.out.println("este");
        else System.out.println("nu este");
    }
    private static int fib(int n){
        if(n==0)return 1;
        if(n==1)return 1;
        return fib(n-1)+fib(n-2);
    }
}