package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*5. Să se calculeze primul număr prim mai mare decât un număr
        dat ca parametru în linia de comandă.
*/

class PrimulNrPrim
{
    public static void main(String args[])
    {
        int nr=Integer.parseInt(args[0]);
        int nrCrt=nr+1;//cautam incepand cu nr+1
        for(;;){
            if(estePrim(nrCrt))
                break;//l-a gasit
            else nrCrt++;
        }
        System.out.println("Primul numar prim mai mare este: "+nrCrt);
    }
    private static boolean estePrim(int x)
    {
        boolean este=true;
        for(int i=2;i<=Math.sqrt(x);i++)
            if(x % i == 0){
                este=false;
                break; }
        return este;
    }
}
