package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*13. Se citeşte un număr natural N. Să se calculeze termenul de
        rang N din şirul lui Fibonacci.
        Şirul lui Fibonacci se defineşte recursiv astfel:
        a0= 1
        a1=1
        an=an-1+an-2 , pentru n>=2
*/

import javax.swing.*;

class Fibonacci
{
    public static void main(String args[])
    {
        int N=Integer.parseInt(JOptionPane.showInputDialog("N="));
        System.out.println(fib(N));
    }
    private static int fib(int n){
        if(n==0)return 1;
        if(n==1)return 1;
        return fib(n-1)+fib(n-2);
    }
}
