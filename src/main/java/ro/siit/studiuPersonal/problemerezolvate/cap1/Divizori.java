package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*4. Să se afişeze toţi divizorii unui număr întreg
    dat ca parametru în linia de comandă.
 */

class Divizori
{
    public static void main(String args[])
    {
        int nr=Integer.parseInt(args[0]);
        for(int i=1; i<=nr; i++)
            if(nr%i==0)System.out.println(i);
    }
}
