package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*8. Se citeşte un număr natural a. Să se afişeze dacă este pătrat
        perfect sau nu.
*/

import javax.swing.*;

class PatratPerfect
{
    public static void main(String args[])
    {
        int a=Integer.parseInt(JOptionPane.showInputDialog("a="));
        double radical=Math.sqrt(a);
        if((int)radical*(int)radical==a)
            System.out.println("este");
        else System.out.println("nu este");
    }
}