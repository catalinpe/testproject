package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*3. Să se calculeze valoare constantei PI, pe baza formulei:
        pi/4=1-1/3+1/5-1/7+1/9-...
        Se vor lua N=3000 de termini în această sumă.
*/

class Pi
{
    public static void main(String args[])
    {
        final int N=3000;//nr. de termeni ce se aduna
        double pi=0;
        for(int i=0;i<N;i++)
            if(i%2==0)pi=pi+4.0/(2*i+1);
            else pi=pi-4.0/(2*i+1);
        System.out.println("PI calculat ca suma de "+N+" termeni: "+pi);
        System.out.println("Constanta PI din clasa Math: "+Math.PI);
    }
}
