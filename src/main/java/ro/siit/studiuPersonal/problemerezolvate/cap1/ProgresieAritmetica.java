package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*12. Se citeşte un număr natural n. Se citesc primul termen şi raţia
        unei progresii aritmetice. Să se calculeze, recursiv, termenul de rang n
        al progresiei.
*/

import javax.swing.*;

class ProgresieAritmetica {
    public static void main(String args[]) {
        double a1 = Double.parseDouble(JOptionPane.showInputDialog("primul termen al progresiei="));
        double r = Double.parseDouble(JOptionPane.showInputDialog("ratia ="));
        int n = Integer.parseInt(JOptionPane.showInputDialog("n ="));
        //calcul termen de rang n:
        double a_n = calcul(a1, r, n);
        System.out.print("Termenul de rang " + n + " este: " + a_n);
    }

    private static double calcul(double primulTermen, double ratia, int n) {
        if (n == 1) return primulTermen;
        return ratia + calcul(primulTermen, ratia, n - 1);
    }
}
