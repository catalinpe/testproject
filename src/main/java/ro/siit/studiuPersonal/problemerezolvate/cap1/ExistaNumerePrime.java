package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*9. Se citesc n1 şi n2 capetele unui interval închis. Să se afişeze
        dacă în intervalul [n1, n2], există cel puţin un număr prim.
*/

import javax.swing.*;

class ExistaNumerePrime {
    public static void main(String args[]) {
        int n1 = Integer.parseInt(JOptionPane.showInputDialog("n1="));
        int n2 = Integer.parseInt(JOptionPane.showInputDialog("n2="));
        boolean exista = false;
        for (int i = n1; i <= n2; i++)
            if (estePrim(i)) {
                exista = true;
                break;
            }
        if (exista) System.out.println("exista");
        else System.out.println("nu exista");
    }

    private static boolean estePrim(int nr) {
        for (int i = 2; i <= Math.sqrt(nr); i++)
            if (nr % i == 0) return false;
        return true;
    }
}
