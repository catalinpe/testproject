package ro.siit.studiuPersonal.problemerezolvate.cap1;

/*7.Se citesc două numere a şi b.Să se afişeze dacă cele două
        numere au acelaşi număr total de divizori.
*/

import javax.swing.*;

class Comparare {
    public static void main(String args[]) {
        int a = Integer.parseInt(JOptionPane.showInputDialog("a="));
        int b = Integer.parseInt(JOptionPane.showInputDialog("b="));
        int na = 2; // numarul de divizori ai lui a
        // orice numar are ca divizori pe 1 si el insusi
        for (int i = 2; i <= a / 2; i++)
            if (a % i == 0) na++;
        int nb = 2; // numarul de divizori ai lui b
        for (int i = 2; i <= b / 2; i++)
            if (b % i == 0) nb++;
        if (na > nb) System.out.println(a + " are mai multi divizori ");
        else if (na == nb) System.out.println("acelasi numar de divizori ");
        else System.out.println(b + " are mai multi divizori ");
    }
}
