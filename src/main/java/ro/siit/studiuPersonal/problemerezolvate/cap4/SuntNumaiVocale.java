package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*7. Se citeşte un şir.
     Să se afişeze dacă şirul conţine numai vocale.
     Se va folosi o metodă separată: suntNumaiVocale().
*/

import javax.swing.*;

class SuntNumaiVocale {
    public static void main(String args[]) {
        String s = JOptionPane.showInputDialog("sir = ");
        System.out.println("are numai vocale = " + suntNumaiVocale(s));
    }

    private static boolean suntNumaiVocale(String s) {
        for (int i = 0; i < s.length(); i++)
            if (esteVocala(s.charAt(i)) == false) return false;
        return true;
    }

    private static boolean esteVocala(char ch) {
        if ((ch == 'a') || (ch == 'A') ||
                (ch == 'e') || (ch == 'E') ||
                (ch == 'i') || (ch == 'I') ||
                (ch == 'o') || (ch == 'O') ||
                (ch == 'u') || (ch == 'U') ||
                (ch == 'ă') || (ch == 'Ă') ||
                (ch == 'î') || (ch == 'Î') ||
                (ch == 'â') || (ch == 'Â')) return true;
        else return false;
    }
}