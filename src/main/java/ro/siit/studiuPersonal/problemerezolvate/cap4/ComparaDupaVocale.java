package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*4. Se citesc două şiruri s1 şi s2.
     Să se afişeze care şir conţine mai multe vocale.
*/

import javax.swing.*;

class ComparaDupaVocale {
    public static void main(String args[]) {
        String s1 = JOptionPane.showInputDialog("sir1 = ");
        String s2 = JOptionPane.showInputDialog("sir2 = ");
        int nv1 = numarVocale(s1);
        int nv2 = numarVocale(s2);
        if (nv1 > nv2) System.out.println("Sir 1 are mai multe vocale ca sir 2");
        else if (nv1 == nv2) System.out.println("numar egal de vocale");
        else System.out.println("Sir 2 are mai multe vocale ca sir 1");
        // ;
    }

    private static int numarVocale(String s) {
        int contor = 0;
        for (int i = 0; i < s.length(); i++)
            if (esteVocala(s.charAt(i)) == true) contor++;
        return contor;
    }

    private static boolean esteVocala(char ch) {
        if (
                (ch == 'a') || (ch == 'A') ||
                        (ch == 'e') || (ch == 'E') ||
                        (ch == 'i') || (ch == 'I') ||
                        (ch == 'o') || (ch == 'O') ||
                        (ch == 'u') || (ch == 'U') ||
                        (ch == 'ă') || (ch == 'Ă') ||
                        (ch == 'î') || (ch == 'Î') ||
                        (ch == 'â') || (ch == 'Â'))
            return true;
        else return false;
    }
}