package ro.siit.studiuPersonal.problemerezolvate.cap4;
/*
Se citeşte un şir s1 de la tastatură.
Să se construiască un alt şir s2, obţinut prin criptarea şirului iniţial după algoritmul lui Cezar.
(La început se convertesc toate literele mari din şirul s1, în litere mici.
Apoi fiecare literă mică din s1 se criptează în şirul s2 astfel: a se
înlocuieşte cu d, b cu e,..., x cu a, y cu b şi z cu c; se observă că
distanţa dintre literă şi litera criptată este 3).
 */

import javax.swing.*;

class CriptareString {
    public static void main(String args[]) {
        String s = JOptionPane.showInputDialog("sir = ");
        s = s.toLowerCase();
        String sCriptat = "";
        for (int i = 0; i < s.length(); i++)
            if (esteLitera(s.charAt(i)))
                sCriptat = sCriptat + literaCriptata(s.charAt(i));
            else sCriptat = sCriptat + s.charAt(i);
        System.out.println("sir criptat = " + sCriptat);
    }

    private static boolean esteLitera(char ch) {
        int cod = (int) ch;
        if ((cod >= (int) 'a') && (cod <= (int) 'z')) return true;
        else return false;
    }

    private static char literaCriptata(char ch) {
        if (ch == 'x') return 'a';
        if (ch == 'y') return 'b';
        if (ch == 'z') return 'c';
//Pentru restul literelor:
        int cod = (int) ch;
        int noulCod = cod + 3;
        return (char) noulCod;
    }
}
