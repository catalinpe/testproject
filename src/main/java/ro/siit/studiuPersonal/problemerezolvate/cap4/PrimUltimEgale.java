package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*1. Se citeşte un şir. Să se afişeze dacă primul caracter este egal cu ultimul caracter.
 */

import javax.swing.*;

class PrimUltimEgale
{
    public static void main(String args[])
    {
        String s=JOptionPane.showInputDialog("sir = ");
        System.out.println(s);

        if(s.charAt(0)==s.charAt(s.length()-1))
            System.out.println("Primul caracter este egal cu ultimul caracter");
        else System.out.println
                ("Primul caracter este diferit de ultimul caracter");
    }
}