package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*2. Se citesc de la tastatură sub formă de şiruri de caractere doi timpi, în formatul hh:mm:ss (ore: minute: secunde).
        Să se afişeze care timp este mai mare.
        Exemplu:
        T1= 5:35:42
        T2= 5:18:50
        Se va afişa: T2 > T1
*/

import javax.swing.*;

import java.util.*;

class ComparaTimpi
{
    public static void main(String args[])
    {
        //definim timp1 si timp2
        String timp1=JOptionPane.showInputDialog ("timp1 (hh:mm:ss) = ");
        String timp2=JOptionPane.showInputDialog ("timp2 (hh:mm:ss) = ");

        //Extragem din fiecare timp, orele, minutele si secundele.
        // Folosim clasa StringTokenizer pentru a extrage acesti atomi.

        //pentru timp1
        StringTokenizer tk=new StringTokenizer(timp1,":");

        int ore1=Integer.parseInt(tk.nextToken());
        int min1=Integer.parseInt(tk.nextToken());
        int sec1=Integer.parseInt(tk.nextToken());

        //Calculam primul timp, in secunde:
        int T1=3600*ore1+60*min1+sec1;

        //pentru timp2 :
        tk=new StringTokenizer(timp2,":");
        int ore2=Integer.parseInt(tk.nextToken());
        int min2=Integer.parseInt(tk.nextToken());
        int sec2=Integer.parseInt(tk.nextToken());

        //Calculam timpul 2, in secunde
        int T2=3600*ore2+60*min2+sec2;

        //verificarea
        if(T1>T2)System.out.println("timp1 > timp2");
        else if(T1==T2)System.out.println("timp1 = timp2");
        else System.out.println("timp1 < timp2");
    }
}