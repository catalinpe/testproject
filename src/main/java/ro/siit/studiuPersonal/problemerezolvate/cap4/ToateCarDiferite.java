package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*6. Se citeşte un şir. Să se afişeze dacă are toate caracterele diferite între ele.
Se va folosi o metodă separată: suntToateDiferite().
*/

import javax.swing.*;

class ToateCarDiferite {
    public static void main(String args[]) {
        String s = JOptionPane.showInputDialog("sir = ");
        if (suntToateDiferite(s) == true)
            System.out.println("Are toate caracterele diferite");
        else System.out.println("Nu are toate caracterele diferite");
    }

    private static boolean suntToateDiferite(String s) {
        //Comparam caracterul curent, cu toate caracterele de dupa el:
        for (int i = 0; i < s.length() - 1; i++)
            for (int j = i + 1; j < s.length(); j++)
                if (s.charAt(i) == s.charAt(j)) return false;
        return true;
    }
}