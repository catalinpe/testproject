package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*5. Se citeşte un şir. Să se afişeze dacă are toate caracterele egale între ele.
Se va folosi o metodă separată: areToateCarEgale().
*/

import javax.swing.*;

class ToateCarEgale {
    public static void main(String args[]) {
        String s = JOptionPane.showInputDialog("sir = ");
        if (areToateCarEgale(s) == true)
            System.out.println("Are toate caracterele egale");
        else System.out.println("Nu are toate caracterele egale");
    }

    private static boolean areToateCarEgale(String s) {
        //Le comparam pe toate, cu primul caracter:
        for (int i = 1; i < s.length(); i++)
            if (s.charAt(i) != s.charAt(0)) return false;
        return true;
    }
}