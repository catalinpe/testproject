package ro.siit.studiuPersonal.problemerezolvate.cap4;

/*3. Se citeşte un şir s1 de la tastatură, să se construiască un alt şir s2: inversul şirului s1.
 */

import javax.swing.*;

class InvString {
    public static void main(String args[]) {
        String s1 = JOptionPane.showInputDialog("sir=");
        String s2 = "";
        int i;
        int L1 = s1.length();//lungimea sirului s1
        for (i = L1 - 1; i >= 0; i--)
            s2 = s2 + s1.charAt(i);
        System.out.println("Inversul sirului " + s1 + " este " + s2);
    }
}