package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*14. Să se dezvolte clasa Timp ce are ca variabile de instanţă trei
        numere întregi: h, min, sec (ore, minute, secunde).
        Ca metode:
        ‐ constructorul;
        ‐ conversieInSecunde() ce returnează timpul curent, exprimat în secunde;
        ‐ compara(), ce are ca parametru un Timp t, şi care returnează
            1 dacă timpul current este mai mare ca t,
            0 dacă cei doi timpi sunt egali , şi
            -1 în caz contrar.
        Dezvoltarea şi utilizarea de clase elementare 29
      Scrieţi şi o clasă de test.
 */

class Timp
{
    private int h; //ore
    private int min;
    private int sec;

    //Constructor
    public Timp(int ore,int m, int s)
    {
        h=ore;
        min=m;
        sec=s;
    }

    public int conversieSecunde()
    {
        return 3600*h+60*min+sec;
    }

    public int compara(Timp t)
    //returneaza 0 daca cei doi timpi sunt egali
    //returneaza 1 daca timpul curent > timpul t dat ca argument
    //returneaza -1 daca timpul curent < timpul t
    {
        int sec1=this.conversieSecunde();//timpul curent, in secunde
        int sec=t.conversieSecunde();//timpul t, in secunde
        if(sec1>sec)return 1;
        else if(sec1==sec)return 0;
        else return -1;
    }
}
class TestTimp
{
    public static void main(String args[])
    {
        /*
        int ora = Integer.parseInt(JOptionPane.showInputDialog("ora ="));
        int minut = Integer.parseInt(JOptionPane.showInputDialog("minutul ="));
        int secunda = Integer.parseInt(JOptionPane.showInputDialog("secunda ="));
        Timp t1=new Timp(ora,minut,secunda);
        ora = ora + 1;
        minut = minut + 1;
        secunda = secunda + 1;
        Timp t2=new Timp(ora,minut,secunda);
        */

        Timp t1=new Timp(1,5,6);
        Timp t2=new Timp(1,5,6);
        int rezultat=t1.compara(t2);
        if(rezultat==1)System.out.println("t1>t2");
        else if(rezultat==0)System.out.println("t1=t2");
        else System.out.println("t1<t2");
    }
}