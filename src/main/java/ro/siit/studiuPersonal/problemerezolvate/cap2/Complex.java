package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*12. Să se dezvolte clasa Complex, ce are variabile de instanţă
private două numere întregi re şi im (partea reală şi partea imaginară a unui număr complex) şi ca metode:
        ‐ constructorul ce face iniţializările;
        ‐ modul(), ce returnează modulul numărului complex;
        ‐ suma(), ce are ca parametru un număr complex c, prin care la
        numărul complex curent se adună numărul complex c (rezultatul se
        depune în numărul curent);
        ‐ produs(), ce are ca parametru un număr complex c, prin care în
        numărul complex curent se depune rezultatul înmulţirii dintre numărul
        complex curent şi numărul complex c;
        ‐ getRe(), ce returnează partea reală a numărului complex;
        ‐ getIm(), ce returnează partea imaginară a numărului complex;
        ‐ equals(), ce redefineşte metoda equals() din clasa Object, prin
        care se compară din punct de vedere al conţinutului, două obiecte
        Complex: obiectul curent şi obiectul dat ca parametru;
        ‐ toString(), ce redefineşte metoda toString() din clasa Object,
        prin care se dă o reprezentare sub formă de String a unui număr complex;
        Scrieţi şi o clasa de test pentru clasa Complex.
 */

class Complex
{
    private double re;
    private double im;

    //Constructor
    public Complex(double x, double y)
    {
        re=x;
        im=y;
    }

    public double getRe()
    {
        return re;
    }

    public double getIm()
    {
        return im;
    }

    public double modul()
    {
        return Math.sqrt(re*re+im*im);
    }

    //adunarea nr. complex curent, cu un alt nr. complex, cu depunerea
    //rezultatului in numarul complex curent:
    public void suma(Complex c)
    {
        re=re+c.re;
        im=im+c.im;
    }

    //inmultirea nr. complex curent, cu un alt nr. complex, cu depunerea
    //rezultatului in numarul complex curent:
    public void produs(Complex c)
    {
        re=re*c.re-im*c.im;
        im=re*c.im+im*c.re;
    }

    //redefinirea metodei equals() din clasa parinte Object:
    //(trebuie sa se pastreze aceeasi semnatura:)

    public boolean equals(Object obj)
    {
        Complex c=(Complex)obj;
        if(c!=null)
            if((re==c.re)&&(im==c.im))return true;
        return false;
    }

    //redefinirea metodei toString() din clasa parinte Object:
    public String toString()
    {
        String s="("+re+","+im+")";
        return s;
    }
}

class TestComplex
{
    public static void main(String args[])
    {
        Complex c1=new Complex(1,1);
        System.out.println("Modulul este= "+c1.modul());
        Complex c2=new Complex(1,1);
        c1.suma(c2);
        System.out.println("suma = "+c1.toString());
        Complex c3=new Complex(1,1);
        System.out.println("sunt egale: "+c2.equals(c3));
    }
}