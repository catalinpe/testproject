package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*3. Folosind clasa BigInteger , să se calculeze şi afişeze valoarea numărului 50!

Se va folosi metoda multiply(), ce are ca parametru un
        număr BigInteger şi scoate ca rezultat un număr de tip BigInteger, care
        reprezintă rezultatul înmulţirii între numărul dat ca parametru şi
        obiectul curent (pentru care se apelează metoda multiply()).
*/

import java.math.*;

class Fact50
{
    public static void main(String args[])
    {
        BigInteger rez=new BigInteger("1");
        for(int i=2;i<=50;i++)
            rez=rez.multiply(new BigInteger(""+i));
        System.out.println(rez);
    }
}