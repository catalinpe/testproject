package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*13. Scrieţi clasa Unghi, ce are ca variabilă de instanţă privată
        un număr întreg x, măsura în grade a unui unghi, şi ca metode:
        ‐ constructorul;
        ‐ suntComplementare(), ce are ca parametru un alt unghi u,
        şi care returnează true dacă unghiul u este complementar cu unghiul curent;
        ‐ conversieRadiani(), ce returnează valoarea exprimată în radiani a unghiului curent x.
        Scrieţi şi o clasă de test pentru clasa Unghi.
 */

class Unghi {
    private int x;

    //Constructor
    public Unghi(int x) {
        this.x = x;
    }

    public boolean suntComplementare(Unghi u) {
        if (this.x + u.x == 90) return true;
        else return false;
    }

    public double conversieRadiani() {
        return (Math.PI * x) / 180;
    }

}

class TestUnghi {
    public static void main(String args[]) {
        /*
        int ungh = Integer.parseInt(JOptionPane.showInputDialog("unghi ="));
        Unghi a = new Unghi(ungh);
        */

        //sau

        /*
        int x=30;
        Unghi a = new Unghi(x);
        */

        //sau

        Unghi a = new Unghi(30);
        System.out.println("Radiani= " + a.conversieRadiani());
        System.out.println("sunt complementare = "+a.suntComplementare(new Unghi(60)));
    }
}