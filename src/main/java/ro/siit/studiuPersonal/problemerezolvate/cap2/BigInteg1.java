package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*2. Folosind clasa BigInteger să se calculeze 2la puterea 1000.
     Vom folosi metoda pow() din această clasă. Să se afişeze şi câte cifre are acest rezultat.

     Metoda pow() are semnătura:
public BigInteger pow(int exponent)
*/

import java.math.*;

class BigInteg1
{
    public static void main(String args[])
    {
        BigInteger baza=new BigInteger("2");
        BigInteger rezultat=baza.pow(1000);
        String s=rezultat.toString();
        System.out.println("rezultat="+s);
        System.out.println("nr. cifre="+s.length());
    }
}