package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*9. Să se construiască clasa Dreptunghi, ce are ca variabile de
        instanţă private, două numere întregi a şi b, ce reprezintă lungimile
        laturilor unui dreptunghi.
        În această clasă avem ca metode:
        ‐ constructorul, ce face iniţializările;
        ‐ metoda calculPerimetru(), ce returnează perimetrul dreptunghiului;
        ‐ metoda calculArie(), ce returnează aria dreptunghiului;
        ‐ metoda estePatrat(), ce returnează true dacă dreptunghiul este pătrat;
        ‐ metoda suntEgale(), ce are ca parametru un dreptunghi d şi
        scoate ca rezultat true dacă dreptunghiul curent (cel pentru care se
        apelează metoda) este egal cu dreptunghiul d.
        Scrieţi şi o clasă de test pentru clasa Dreptunghi.
*/

class Dreptunghi
{
    private int a;
    private int b;

    //Constructor (face initializarile)
    public Dreptunghi(int x,int y)
    {
        a=x;
        b=y;
    }

    //Metoda calculPerimetru
    public int calculPerimetru()
    {
        return 2*(a+b);
    }

    //Metoda calculArie
    public int calculArie()
    {
        return a*b;
    }

    //Metoda estePatrat
    public boolean estePatrat()
    {
        if(a==b)return true;
        else return false;
    }

    //Metoda suntEgale
    public boolean suntEgale(Dreptunghi d)
    {
        if ((this.a==d.a)&&(this.b==d.b))return true;
        else return false;
    }
}
class TestDreptunghi
{
    public static void main (String args[])
    {
        Dreptunghi d=new Dreptunghi(5,7);
        System.out.println("Primetrul este "+d.calculPerimetru());
        System.out.println("Aria este "+d.calculArie());
        System.out.println("Dreptunghiul este patrat= "+d.estePatrat());
        Dreptunghi d1=new Dreptunghi(5,7);
        System.out.println("sunt egale= "+d.suntEgale(d1));
    }
}