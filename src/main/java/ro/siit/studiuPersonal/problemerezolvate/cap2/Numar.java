package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*8. Scrieţi clasa Numar ce are ca variabilă de instanţă privată un număr întreg nr, şi ca metode:
        ‐ constructorul, ce iniţializează variabila nr;
        ‐ getNr(), ce returnează valoarea variabilei private nr;
        ‐ metoda estePatratPerfect() ce returnează true dacă nr este pătrat perfect şi false în caz contrar;
        ‐ metoda estePrim() ce returnează true dacă nr este prim şi false în caz contrar;
        ‐ metoda afisareDivizori() ce afişează divizorii numărului nr.
        Scrieţi şi o clasă de test pentru clasa Numar.
*/

class Numar
{
    private int nr;

    public Numar(int x)
    {
        nr=x;
    }

    public int getNr()
    {
        return nr;
    }

    public void afisareDivizori()
    {
        //nr. 1, este sigur divizor:
        System.out.println("1");
        //Cautam divizorii intre 2 si jumatatea numarului:
        for(int i=2;i<=nr/2;i++)
            if(nr%i==0)System.out.println(i);
        //numarul se divide cu el insusi:
        System.out.println(nr);
    }

    public boolean estePrim()
    {
        boolean este = true;
        for(int i=2;i<=Math.sqrt(nr);i++)
            if(nr%i==0){
                este = false;
                break;
            }
        return este;
    }

    public boolean estePatratPerfect()
    {
        int radical=(int)Math.sqrt(nr);
        if(radical*radical==nr)return true;
        else return false;
    }
}

class TestNumar
{
    public static void main (String args[])
    {
        Numar n=new Numar(25);
        System.out.println("Divizorii numarului "+n.getNr());
        n.afisareDivizori();
        System.out.println("este patrat perfect="+n.estePatratPerfect());
        if(n.estePrim()==true)System.out.println(n.getNr()+" este prim.");
        else System.out.println(n.getNr()+" nu este prim.");
    }
}