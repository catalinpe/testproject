package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*11. Să se dezvolte clasa TreiNumere, ce are variabile de instanţă
        trei numere întregi a,b şi c, şi ca metode:
        ‐ constructorul ce face iniţializările;
        ‐ metoda maxim(), ce returnează maximul dintre a, b şi c;
        ‐ metoda suntPitagorice(), ce returnează true, dacă a, b, c sunt numere pitagorice.
        Scrieţi şi o clasă de test pentru clasa TreiNumere.
 */
class TreiNumere
{
    private int a,b,c;

    //Constructor
    public TreiNumere(int n1, int n2, int n3)
    {
        a=n1;
        b=n2;
        c=n3;
    }

    //Metoda maxim
    public int maxim()
    {
        int max=a;
        if(b>max)max=b;
        if(c>max)max=c;
        return max;
    }

    //Metoda suntPitagorice
    public boolean suntPitagorice()
    {
        if((a*a==b*b+c*c)||(b*b==a*a+c*c)||(c*c==a*a+b*b))
            return(true);
        else return(false);
    }
}

class Test3Numere
{
    public static void main(String args[])
    {
        /*
        int a1=Integer.parseInt(JOptionPane.showInputDialog("a1="));
        int b1=Integer.parseInt(JOptionPane.showInputDialog("b1="));
        int c1=Integer.parseInt(JOptionPane.showInputDialog("c1="));
        TreiNumere t=new TreiNumere(a1,b1,c1);
        */

        TreiNumere t=new TreiNumere(3,5,4);
        System.out.println("maximul este: "+t.maxim());
        if(t.suntPitagorice())
            System.out.println("Sunt numere pitagorice");
        else System.out.println("Nu sunt numere pitagorice");
    }
}