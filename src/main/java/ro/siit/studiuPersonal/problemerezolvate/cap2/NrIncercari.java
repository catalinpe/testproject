package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*1. Să se afişeze din câte încercări se generează trei numere
        aleatoare egale, în gama 0..19.
*/

import java.util.*;

class NrIncercari{
    public static void main(String args[]){
        final int GAMA=20;
        Random r=new Random();
        int contor=0;
        for(;;){
            int a=r.nextInt(GAMA);
            int b=r.nextInt(GAMA);
            int c=r.nextInt(GAMA);
            contor++;
            if((a==b)&&(b==c))break;
        }
        System.out.println(contor);
    }
}