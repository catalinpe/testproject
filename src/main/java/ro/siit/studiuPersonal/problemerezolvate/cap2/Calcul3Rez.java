package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*7. Pentru trei numere întregi a, b şi c, să se calculeze
maximul,
minimul şi
media aritmetică a celor trei numere, folosind o metodă
        separată ce are ca parametrii trei numere întregi şi care returnează trei
        rezultate: maximul, minimul şi media aritmetică a celor trei numere.
 */

class Calcul3Rez
{
    public static void main(String args[])
    {
        //Initializam in mod direct cele trei numere:
        int a=7;
        int b=8;
        int c=4;
        Triplet t=getRezultate(a,b,c);
        //afisam numerele:
        System.out.println("minim="+t.x);
        System.out.println("maxim="+t.y);
        System.out.println("medie="+t.z);
    }
    //metoda ce returneaza cele trei rezultate:
    private static Triplet getRezultate(int a, int b, int c)
    {
        int min=a; if(b<min)min=b; if(c<min)min=c;
        int max=a; if(b>max)max=b; if(c>max)max=c;
        double medie=(a+b+c)/3.0;
        //formez un obiect din clasa Triplet:
        Triplet t=new Triplet(min, max, medie);
        return t;
    }
}
class Triplet
{
    public int x;
    public int y;
    public double z;
    public Triplet(int a, int b, double c)
    {
        x=a;
        y=b;
        z=c;
    }
}