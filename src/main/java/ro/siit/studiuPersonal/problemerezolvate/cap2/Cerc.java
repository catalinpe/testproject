package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*15. Să se construiască clasa Cerc, ce are ca variabilă de instanţă privată,
      un număr întreg r, ce reprezintă raza unui cerc.
      În această clasă avem ca metode:
        ‐ constructorul, ce face iniţializarea razei;
        ‐ metoda calculPerimetru(), ce returnează perimetrul cercului;
        ‐ metoda calculArie(),ce returnează aria cercului;
        Scrieţi şi o clasă de test pentru clasa Cerc.
*/

class Cerc
{
    private int raza;

    //Constructor
    public Cerc(int x)
    {
        raza=x;
    }

    public double calculPerimetru()
    {
        return 2*Math.PI*raza;
    }

    public double calculArie()
    {
        return Math.PI*raza*raza;
    }
}

class TestCerc
{
    public static void main (String args[])
    {
        /*
        int cerc = Integer.parseInt(JOptionPane.showInputDialog("raza ="));
        Cerc c=new Cerc(cerc);
         */

        //sau

        Cerc c=new Cerc(3);
        System.out.println("Perimetru= "+c.calculPerimetru());
        System.out.println("Aria= "+c.calculArie());
    }
}