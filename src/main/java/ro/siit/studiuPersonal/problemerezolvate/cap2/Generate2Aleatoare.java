package ro.siit.studiuPersonal.problemerezolvate.cap2;

/*6. Să se scrie o metodă ce are ca parametru un număr natural N;
        metoda returnează două numere aleatoare diferite, în gama 0...N-1.
        Daţi şi un exemplu de aplicaţie în care se foloseşte această metodă.
*/

import java.util.*;

class Generare2Aleatoare
{
    public static void main(String args[])
    {
        final int N=100;
        Dublet d=getDouaNumereDiferite(N);
        //afisam numerele:
        System.out.println(d.x);
        System.out.println(d.y);
    }
        //metoda ce returneaza doua numere aleatoare //diferite in gama 0..N-1
    private static Dublet getDouaNumereDiferite(int N)
    {
        Random r=new Random();
        int n1=r.nextInt(N);//primul numar
        int n2;
        for(;;){
            n2=r.nextInt(N);
            if(n1!=n2)break;
        }
        //formez un obiect din clasa Dublet:
        Dublet d2=new Dublet(n1,n2);
        return d2;
    }
}
class Dublet
{
    public int x;
    public int y;
    public Dublet(int a, int b)
    {
        x=a;
        y=b;
    }
}