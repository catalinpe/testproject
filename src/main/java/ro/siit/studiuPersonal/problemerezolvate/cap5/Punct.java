package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Să se construiască clasa Punct ce are ca variabile de instanţă două numere întregi x şi y
– coordonatele unui punct în plan, şi ca metode:
        ‐ Constructorul ce face iniţializările;
        ‐ getX() ce returnează valoarea coordonatei x
        ‐ getY() ce returnează valoarea coordonatei y
        ‐ afisare() în care se afişează coordonatele punctului din clasa Punct

Din clasa Punct se derivează două clase: PunctColor şi Punct3D.

Clasa PunctColor faţă de clasa Punct are în plus o variabilă de instanţă
        în care este memorată culoarea punctului, un nou constructor în care
        este iniţializată şi culoarea, metoda getCuloare() ce returnează
        culoarea, şi redefineşte metoda clasei de bază, afişare(), afişând pe
        lângă coordonatele x şi y şi culoarea.

Clasa Punct3D, ce reprezintă un punct în spaţiu, faţă de clasa Punct
        are în plus o variabilă de instanţă z, un nou constructor în care sunt
        iniţializate toate cele trei coordonate, metoda getZ() ce returnează
        valoarea coordonatei z, şi redefineşte metoda clasei de bază, afişare(),
        afişând pe lângă coordonatele x şi y şi coordonata z.

Folosind aceste trei clase se va dezvolta o aplicaţie în care se vor citi
        de la tastatură N puncte (N- citit anterior), de tipul PunctColor sau
        Punct3D. Pentru fiecare punct, în momentul citirii utilizatorul
        aplicaţiei va specifica dacă va introduce un PunctColor sau un
        Punct3D. Cele N puncte se vor memora într-un vector de obiecte de
        tipul Punct (clasa de bază). În final se vor afişa pentru fiecare punct
        din cele N informaţiile memorate (pentru fiecare punct se va apela
        metoda afişare()). Această aplicaţie ilustrează conceptul de
        polimorfism (Compilatorul ştie la rulare ce versiune de metodă
        afişare() să apeleze).
 */

import javax.swing.*;

class Punct {
    private int x;//coordonata x a punctului
    private int y;

    public Punct(int x0, int y0) {
        x = x0;
        y = y0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void afisare() {
        System.out.println("x=" + x);
        System.out.println("y=" + y);
    }
}

class PunctColor extends Punct {
    private String culoare;

    public PunctColor(int x, int y, String culoare) {
        super(x, y);
        this.culoare = culoare;
    }

    public String getCuloare() {
        return culoare;
    }

    public void afisare() {
//System.out.println("x="+x);GRESIT! x - este var. privata !
        System.out.println("x=" + getX());
        System.out.println("y=" + getY());
        ;
        System.out.println("culoare=" + culoare);
    }
}

class Punct3D1 extends Punct {
    private int z;

    public Punct3D1(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public int getZ() {
        return z;
    }

    public void afisare() {
        System.out.println("x=" + getX());
        System.out.println("y=" + getY());
        System.out.println("z=" + z);
    }
}

class AfisarePuncte {
    public static void main(String args[]) {
        int N;//numarul de puncte
        N = Integer.parseInt(JOptionPane.showInputDialog("N="));
        Punct p[] = new Punct[N];//vectorul de obiecte Punct (clasa de
//baza)
        int i;
        for (i = 0; i < N; i++) {
            String sRaspuns = JOptionPane.showInputDialog("Tip punct(0, 1, 2) :");
            int raspuns = Integer.parseInt(sRaspuns);
            int x = Integer.parseInt(JOptionPane.showInputDialog("x="));
            int y = Integer.parseInt(JOptionPane.showInputDialog("y="));
            if (raspuns == 0) {
//citeste un Punct:
                p[i] = new Punct(x, y);
            } else if (raspuns == 1) {
//citeste un PunctColor:
                String culoare = JOptionPane.showInputDialog("culoare=");
                p[i] = new PunctColor(x, y, culoare);
            } else if (raspuns == 2) {
//citeste un Punct3D:
                int z = Integer.parseInt(JOptionPane.showInputDialog("z="));
                p[i] = new Punct3D1(x, y, z);
            }
        }//for
//Afisare vector:
        for (i = 0; i < N; i++)
            p[i].afisare();
    }
}
