package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Să se construiască clasa Cerc, ce are ca variabilă de instanţă privată, un număr întreg r, ce reprezintă raza unui cerc.
În această clasă avem ca metode:
        ‐ constructorul, ce face iniţializarea razei;
        ‐ getRaza(), ce returnează raza;
        ‐ calculArie(),ce returnează aria cercului;
        ‐ suntEgale(), ce are ca parametru un Cerc c, şi care returnează
        true dacă cercul curent este egal cu cercul c (au aceeaşi rază).
        ‐ afisare(), ce afişează raza cercului.

Din clasa Cerc se va deriva clasa CercExtins, în care se vor adăuga ca
        variabile de instanţă x şi y: coordonatele centrului şi se vor redefini
        metodele suntEgale() (cercurile sunt egale când au aceeaşi rază şi
        aceleaşi coordonate ale centrului), şi afisare() (pe lângă rază, va afişa
        şi coordonatele centrului)

Scrieţi şi o clasă de test pentru clasa CercExtins.
*/

class Cerc
{
    private int raza;
    public Cerc(int x)
    {
        raza=x;
    }
    public int getRaza()
    {
        return raza;
    }
    public double calculArie()
    {
        return Math.PI*raza*raza;
    }
    public boolean suntEgale(Cerc c)
    {
        if(this.raza==c.raza)return true;
        else return false;
    }
    public void afisare()
    {
        System.out.println("raza="+raza);
    }
}

class CercExtins extends Cerc
{
    private int x;
    private int y;
    public CercExtins(int r,int x0, int y0 )
    {
        super(r);
        x=x0;
        y=y0;
    }
    public boolean suntEgale(CercExtins c)
    {
        if((this.getRaza()==c.getRaza())&&(this.x==c.x)&&(this.y==c.y))
            return true;
        else return false;
    }
    public void afisare()
    {
        System.out.println("raza="+this.getRaza());
        System.out.println("x="+x);
        System.out.println("y="+y);
    }
}

class TestCercExtins
{
    public static void main (String args[])
    {
        CercExtins c=new CercExtins(3,0,1);
        System.out.println("Aria= "+c.calculArie());

        CercExtins c1=new CercExtins(3,0,10);
        System.out.println("Sunt egale= "+c.suntEgale(c1));
    }
}