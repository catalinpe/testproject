package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Folosind cele două clase anterioare, Cerc şi CercExtins,
să se dezvolte o aplicaţie în care se vor citi N cercuri (de tipul Cerc sau CercExtins),
ce se memorează într-un vector.
Citirea unui obiect de tip Cerc sau CercExtins este dată de valoarea 0 sau 1 a unui număr aleator generat.
*/

import java.util.*;
import javax.swing.*;

class AfisareCercuri
{
    public static void main (String args[])
    {
        final int N=3;//numarul de cercuri
        Cerc c[]=new Cerc[N];//vectorul de obiecte Cerc (clasa de baza)
        Random r=new Random();
        int i;
        for(i=0;i<N;i++){
            int caz=r.nextInt(2);
            if(caz==0){
//citeste un Cerc:
                String s_raza=JOptionPane.showInputDialog("raza=");
                c[i]=new Cerc(Integer.parseInt(s_raza));
            }else{
//citeste un CercExins:
                String s_raza;
                s_raza=JOptionPane.showInputDialog("raza=");
                String s_x=JOptionPane.showInputDialog("x=");
                String s_y=JOptionPane.showInputDialog("y=");
                c[i]=new CercExtins(Integer.parseInt(s_raza),
                        Integer.parseInt(s_x),
                        Integer.parseInt(s_y));
            }
        }//for
//Afisare vector:
        for(i=0;i<N;i++)
            c[i].afisare();
    }
}
