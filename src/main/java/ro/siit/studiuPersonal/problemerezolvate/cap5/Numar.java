package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Să se construiască clasa Număr ce are ca variabilă de instanţă un număr întreg a, şi ca metode:
        ‐ constructorul;
        ‐ afisare().

Din clasa Numar, se va deriva clasa DouaNumere, în care se va
        adăuga variabila de instanţă b (ce reprezintă al doilea număr), şi se va
        modifica constructorul şi metoda afisare().

Folosind cele două clase, se va dezvolta o aplicaţie în care se generează un număr aleator 0 sau 1.
Dacă este 0, se va instanţia un obiect din clasa Numar (prin citire de la tastatură),
dacă este 1, se va instanţia un obiect din clasa DouăNumere (tot prin citire de la tastatură).
Pentru obiectul instanţiat se va apela metoda afisare().
*/

import java.util.*;
import javax.swing.*;

class Numar
{
    private int a;
    public Numar(int x)
    {
        a=x;
    }
    public void afisare()
    {
        System.out.println("a="+a);
    }
}

class DouaNumere extends Numar
{
    private int b;
    public DouaNumere(int a,int b )
    {
        super(a);
        this.b=b;
    }
    public void afisare()
    {
//afisare a:
        super.afisare();//Cu cuv. cheie super se apeleaza metoda
                        //clasei de baza
//afisare b:
        System.out.println("b="+b);
    }
}
class AfisareNumere
{
    public static void main (String args[])
    {
        Numar n;
        Random r=new Random();
        int caz=r.nextInt(2);
        if(caz==0){
//citeste un numar:
            int a=Integer.parseInt(JOptionPane.showInputDialog("a="));
            n=new Numar(a);
        }else
        {
//citeste doua numere:
            int a=Integer.parseInt(JOptionPane.showInputDialog("a="));
            int b=Integer.parseInt(JOptionPane.showInputDialog("b="));
            n=new DouaNumere(a,b);
        }
//Afisare:
        n. afisare();//Numai la executie se stie care versiune de metoda
                     // afisare() se va apela
    }
}
