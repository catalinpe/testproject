package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*
Să se dezvolte clasa Persoana ce are ca variabile de instanţă
numele şi prenumele unei persoane şi vârsta ei, şi ca metode:
‐ constructorul ce face iniţializările;
‐ getNume(), ce returnează numele;
‐ getPrenume(), ce returnează prenumele;
‐ afisare(), ce afişează informaţiile despre persoană.

Din clasa Persoană se va deriva clasa Student, ce are în plus ca
variabile de instanţă, numele facultaţii pe care o urmează şi numărul
matricol.
În clasa Student se va dezvolta un nou constructor şi se va
redefini metoda afisare(). Se vor adăuga în plus metodele:
‐ getFacultate();
‐ getNumărMatricol().

Se va dezvolta o aplicaţie în care se vor citi de la tastatură N= 10
studenţi, ce se vor memora într-un vector.
Se vor afişa câţi studenţi au prenumele ”Ion”.
*/

import javax.swing.*;

class Persoana {
    private String nume;
    private String prenume;
    private int varsta;

    public Persoana(String n, String p, int v) {
        nume = n;
        prenume = p;
        varsta = v;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void afisare() {
        System.out.println(nume + " " + prenume + " : " + varsta);
    }
}

class Student extends Persoana {
    private String numeFacultate;
    private int nrMatricol;

    public Student(String n, String p, int v, String facult, int nrMatr) {
        super(n, p, v);
        numeFacultate = facult;
        nrMatricol = nrMatr;
    }

    public String getFacultate() {
        return numeFacultate;
    }

    public int getNumarMatricol() {
        return nrMatricol;
    }
}

class TestStudenti {
    public static void main(String args[]) {
        final int N = 2;
        int i;
        Student s[] = new Student[N];
        for (i = 0; i < N; i++) {
            String nume = JOptionPane.showInputDialog("nume=");
            String prenume = JOptionPane.showInputDialog("prenume=");
            int varsta = Integer.parseInt(JOptionPane.showInputDialog("varsta="));
            String facultate = JOptionPane.showInputDialog("facultate=");
            int nrMatr = Integer.parseInt(JOptionPane.showInputDialog("nr. matricol="));
            s[i] = new Student(nume, prenume, varsta, facultate, nrMatr);
        }
        int contor_ion = 0;
        for (i = 0; i < N; i++) {
            String prenumeCrt = s[i].getPrenume();
            if (prenumeCrt.compareTo("Ion") == 0) contor_ion++;
        }
        System.out.println(contor_ion);
    }
}

