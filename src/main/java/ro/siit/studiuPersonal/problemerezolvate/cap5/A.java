package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Să se dezvolte clasa abstractă A în care sunt definite două metode:
        ‐ metoda abstractă calcul();
        ‐ metoda durataCalcul() ce returnează durata exprimată în milisecunde, a execuţiei metodei calcul();

Din clasa abstractă A, se va deriva clasa B ce conţine implementarea metodei calcul().
Se va dezvolta şi o clasă de test, pentru clasa derivată B.
*/

abstract class A
{
    abstract public void calcul(int N);
    public long durataCalcul(int N){
        long t1=System.currentTimeMillis();
        calcul(N);
        long t2=System.currentTimeMillis();
        return (t2-t1);
    }
}

class B extends A
{
    public void calcul(int N)
    {
//Calculeaza N*N*N produse
        int i,j,k;
        long rezultat;
        for(i=1;i<=N;i++)
            for(j=1;j<=N;j++)
                for(k=1;k<=N;k++)
                    rezultat=i*j*k;
    }
}

class Test
{
    public static void main(String args[])
    {
        final int N=1000;
        B b=new B();
        System.out.println("durata calcul = "+b.durataCalcul(N)+" ms.");
    }
}
