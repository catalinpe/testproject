package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*
Să se construiască clasa Punct3D, folosită pentru a modela un punct în spaţiu,
ce are ca variabile de instanţă x, y, z, coordonatele unui punct în spaţiu.

Ca metode:
‐ constructorul;
‐ muta(), ce are trei parametrii dx, dy şi dz, pe baza cărora noile coordonate ale punctului devin: x+dx, y+dy, z+dz;
‐ compara(), ce are ca parametru un punct p, şi care
    -returnează true, dacă punctul curent (cel referit prin this ) este egal cu punctul p, şi false în caz contrar;
‐ distanta(), ce are ca parametru un punct p, şi care
    -returnează distanţa între punctul curent şi punctul p;
‐ getX() ce returnează valoarea coordonatei x;
‐ getY() ce returnează valoarea coordonatei y;
‐ getZ() ce returnează valoarea coordonatei z;
‐ afisare() ce afişează coordonatele punctului.

Pe baza clasei Punct3D, se va dezvolta clasa Punct3DColor, în care se va adăuga:
    -o nouă variabilă de instanţă de tipul String: culoarea punctului şi
    -o nouă metodă getCuloare() ce returnează culoarea punctului.
Se vor redefini metodele compara() şi afişare() şi noul constructor.
Să se scrie şi o clasă de test pentru clasa Punct3DColor.
 */

class Punct3D
{
    private int x;//coordonata x a punctului
    private int y;
    private int z;

    public Punct3D(int x,int y, int z )
    {
        this.x=x;
        this.y=y;
        this.z=z;
    }
    public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    public int getZ()
    {
        return z;
    }
    public void afisare()
    {
        System.out.println("x="+x);
        System.out.println("y="+y);
        System.out.println("z="+z);
    }
    public void muta(int dx, int dy, int dz)
    {
        x=x+dx;
        y=y+dy;
        z=z+dz;
    }
    public boolean compara(Punct3D p)
    {
        if((x==p.x)&&(y==p.y)&&(z==p.z))
            return true;
        else return false;
    }
    public double distanta(Punct3D p)
    {
        double dx=this.x-p.x;
        double dy=this.y-p.y;
        double dz=this.z-p.z;
        double dist=Math.sqrt(dx*dx+dy*dy+dz*dz);
        return dist;
    }
}
class Punct3DColor extends Punct3D
{
    private String culoare;
    public Punct3DColor(int x, int y, int z, String culoare)
    {
        super(x,y,z);
        this.culoare=culoare;
    }
    public String getCuloare()
    {
        return culoare;
    }
    public void afisare()
    {
        System.out.println("x="+getX());
        System.out.println("y="+getY());
        System.out.println("z="+getZ());
        System.out.println("culoare="+culoare);
    }
    public boolean compara(Punct3DColor p)
    {
        if((this.getX()==p.getX())&&
                (this.getY()==p.getY())&&
                (this.getZ()==p.getZ())&&
                (this.culoare==p.culoare))
            return true;
        else return false;
    }
}
class TestPuncte
{
    public static void main (String args[])
    {
        Punct3DColor p=new Punct3DColor(0,1,2,"negru");
        p.muta(1,1,1);
        p.afisare();
    }
}
