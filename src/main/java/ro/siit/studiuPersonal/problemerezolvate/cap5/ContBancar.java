package ro.siit.studiuPersonal.problemerezolvate.cap5;

/*Să se construiască clasa ContBancar,
folosită pentru a modela un cont bancar,
ce are ca variabilă de instanţă privată, variabila suma, (suma de bani din cont).

Ca metode:
        ‐ constructorul;
        ‐ adauga(), ce are ca parametru un număr real x, valoarea ce se adaugă în cont;
        ‐ extrage(), ce are ca parametru un număr real x, valoarea ce se extrage din cont,
        şi care scoate ca rezultat true, dacă se poate face extragerea (suma >= x), şi false în caz contrar (suma < x);
        ‐ getSuma(), ce returnează valoarea variabilei private suma;
        ‐ afisare(), ce afişează valoarea sumei de bani din cont.

Pe baza clasei ContBancar se va dezvolta prin derivare (moştenire) clasa ContBancarExtins, în care se va adăuga:
        - o nouă variabilă de instanţă: rata dobânzii anuale şi
        - o nouă metodă: adaugaDobandaLunara(), ce adaugă în cont dobânda calculată după trecerea unei luni.
În clasa ContBancarExtins
        -se va redefini şi metoda afisare(), astfel încât să se afişeze şi rata dobânzii.
        -se va defini constructorul, prin care se iniţializează suma de bani din cont şi rata dobânzii.
Să se scrie şi o clasă de test pentru clasa ContBancarExtins.
 */

class ContBancar {
    private double suma;

    public ContBancar(double S) {
        suma = S;
    }

    public void adauga(double S) {
        suma = suma + S;
    }

    public boolean extrage(double S) {
        if (S > suma) return false;
        suma = suma - S;
        return true;
    }

    public double getSuma() {
        return suma;
    }

    public void afisare() {
        System.out.println("suma=" + suma);
    }
}

class ContBancarExtins extends ContBancar {
    private double rd;//rata dobanzii anuale

    public ContBancarExtins(double S, double rata) {
        super(S);
        rd = rata;
    }

    public void adaugaDobandaLunara() {
        double S = this.getSuma();
        double dobanda = S * rd / 12;
        this.adauga(dobanda);
    }

    public void afisare() {
        System.out.println("suma=" + this.getSuma());
        System.out.println("rata dobanzii=" + rd);
    }
}

class TestCont {
    public static void main(String args[]) {
        ContBancarExtins c = new ContBancarExtins(1000, 0.12);
        c.adauga(1000);
        c.adaugaDobandaLunara();
        c.afisare();
    }
}