package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

//https://beginnersbook.com/2017/09/java-program-to-add-two-numbers/
/*
Java Program to Add two Numbers
We specify the value of both the numbers in the program itself.

Second Example: Sum of two numbers using Scanner
The scanner allows us to capture the user input so that we can get the values of both the numbers from user.
The program then calculates the sum and displays it.
 */

import java.util.Scanner;

public class AddTwoNumbers2 {

    public static void main(String[] args) {

        int num1, num2, sum;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter First Number: ");
        num1 = sc.nextInt();

        System.out.println("Enter Second Number: ");
        num2 = sc.nextInt();

        sc.close();
        sum = num1 + num2;
        System.out.println("Sum of these numbers: "+sum);
    }
}
