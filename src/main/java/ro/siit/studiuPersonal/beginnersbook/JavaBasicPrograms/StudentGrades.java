package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2017/09/java-program-to-calculate-and-display-student-grades/

Java Program to calculate and display Student Grades

This program calculates the grade of a student based on the marks entered by user in each subject.
Program prints the grade based on this logic.
If the average of marks is >= 80 then prints Grade ‘A’
If the average is <80 and >=60 then prints Grade ‘B’
If the average is <60 and >=40 then prints Grade ‘C’
else prints Grade ‘D’
 */

import java.util.Scanner;

public class StudentGrades {
    public static void main(String args[])
    {
        /* This program assumes that the student has 6 subjects,
         * thats why I have created the array of size 6. You can
         * change this as per the requirement.
         */
        int marks[] = new int[6];
        int i;
        float total=0, avg;
        Scanner scanner = new Scanner(System.in);


        for(i=0; i<6; i++) {
            System.out.print("Enter Marks of Subject"+(i+1)+":");
            marks[i] = scanner.nextInt();
            total = total + marks[i];
            //System.out.println("Total marks is: "+total);
        }
        scanner.close();

        //Calculating average here
        avg = total/6;
        System.out.print("The student Grade is: ");
        if(avg>=80)
        {
            System.out.print("A");
            //System.out.println("Media este: "+avg);
        }
        else if(avg>=60 && avg<80)
        {
            System.out.print("B");
            //System.out.println("Media este: "+avg);
        }
        else if(avg>=40 && avg<60)
        {
            System.out.print("C");
            //System.out.println("Media este: "+avg);
        }
        else
        {
            System.out.print("D");
            //System.out.println(", iar media este: "+avg);
        }
    }
}
