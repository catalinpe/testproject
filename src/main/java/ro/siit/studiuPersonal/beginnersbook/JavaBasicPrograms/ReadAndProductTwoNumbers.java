package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2017/09/java-program-to-multiply-two-numbers/

The program takes two integer numbers (entered by user) and displays the product of these numbers

Program to read two integer and print product of them
This program asks user to enter two integer numbers and displays the product.

In order to read the input provided by user, we first create the object of Scanner by passing System.in as parameter.
Then we are using nextInt() method of Scanner class to read the integer.
 */

import java.util.Scanner;

public class ReadAndProductTwoNumbers {
    public static void main(String[] args) {

        /* This reads the input provided by user
         * using keyboard
         */
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter first number: ");

        // This method reads the number provided using keyboard
        int num1 = scan.nextInt();

        System.out.print("Enter second number: ");
        int num2 = scan.nextInt();

        // Closing Scanner after the use
        scan.close();

        // Calculating product of two numbers
        int product = num1*num2;

        // Displaying the multiplication result
        System.out.println("Output: "+product);
    }
}
