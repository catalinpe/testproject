package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

//https://beginnersbook.com/2017/09/java-program-to-add-two-numbers/
/*
Java Program to Add two Numbers
We specify the value of both the numbers in the program itself.
 */

public class AddTwoNumbers {

    public static void main(String[] args) {

        int num1 = 5, num2 = 15, sum;
        sum = num1 + num2;

        System.out.println("Sum of these numbers: "+sum);
    }
}