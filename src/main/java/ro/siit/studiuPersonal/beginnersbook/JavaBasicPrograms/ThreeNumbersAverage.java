package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2020/01/java-program-to-find-average-of-3-numbers/

Java Program to find the average of 3 numbers

In this example, we are taking input from the user and calculating the average of entered numbers using “/” operator.
The reason why we are using double as data type because a user can enter any data type number such as int, float,
long & double, since double can hold the values of all these data types,
it is important to declare variables as double data type.

However if you are not taking input from user then you can declare the data type based on the number value,
for example if you want to find out the average of three integer numbers then you are declare
num1, num2 & num3 as int but you have to use double as the return type of the avr method
because the average of 3 int numbers can be a decimal value.
 */

import java.util.Scanner;

public class ThreeNumbersAverage {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the first number: ");

        double num1 = scan.nextDouble();
        System.out.print("Enter the second number: ");

        double num2 = scan.nextDouble();
        System.out.print("Enter the third number: ");

        double num3 = scan.nextDouble();
        System.out.print("The average of entered numbers is:" + avr(num1, num2, num3) );

        scan.close();
    }

    public static double avr(double a, double b, double c)
    {
        return (a + b + c) / 3;
    }

}
