package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/08/java-program-to-find-quotient-and-remainder/

Program to find Quotient and Remainder
In the following program we have two integer numbers num1 and num2 and
we are finding the quotient and remainder when num1 is divided by num2,
so we can say that num1 is the dividend here and num2 is the divisor.
 */

import java.util.Scanner;

public class QuotientAndRemainder {
    public static void main(String[] args) {
        /*
        int num1 = 15, num2 = 2;
        int quotient = num1 / num2;
        int remainder = num1 % num2;
        System.out.println("Quotient is: " + quotient);
        System.out.println("Remainder is: " + remainder);
        */

        //my version
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the first number: ");

        int num1 = scan.nextInt();
        System.out.print("Enter the second number: ");

        int num2 = scan.nextInt();

        int quotient = num1 / num2;
        int remainder = num1 % num2;
        System.out.println("Quotient is: " + quotient);
        System.out.println("Remainder is: " + remainder);
    }
}
