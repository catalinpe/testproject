package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/07/java-program-to-calculate-simple-interest/

Java Program to Calculate Simple Interest

Simple Interest Formula

Simple Interest = (P × R × T)/100
P is Principal amount.
R is rate per annum.
T is time in years.

For example:
Let’s say a man deposit 2000 INR in bank account
at a interest rate of 6% per annum
for 3 years,
calculate the simple interest at the end of 3 years.

Simple interest = 2000*6*3/100 = 360 INR

In this example we are taking the values of p, r and t from user and then
we are calculating the simple interest based on entered values.
 */

import java.util.Scanner;

public class SimpleInterest {
    public static void main(String args[])
    {
        float p, r, t, sinterest;

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the Principal : ");
        p = scan.nextFloat();

        System.out.print("Enter the Rate of interest : ");
        r = scan.nextFloat();

        System.out.print("Enter the Time period : ");
        t = scan.nextFloat();

        scan.close();

        sinterest = (p * r * t) / 100;
        System.out.print("Simple Interest is: " +sinterest);

        //varianta mea
        /*

        float principal, rate, timeperiod, simpleinterest;

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter the Principal : ");
        principal = scan.nextFloat();
        System.out.println("The principal is: "+principal);

        System.out.print("Enter the Rate of interest : ");
        rate = scan.nextFloat();
        System.out.println("The rate is: "+rate);

        System.out.print("Enter the Time period : ");
        timeperiod = scan.nextFloat();
        System.out.println("The time period is: "+timeperiod);

        scan.close();

        simpleinterest = (principal * rate * timeperiod) / 100;
        System.out.print("Simple Interest is: " +simpleinterest);

         */
    }
}
