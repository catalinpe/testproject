package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2017/09/java-program-to-multiply-two-numbers/

Read two integer or floating point numbers and display the multiplication
This programs allows you to enter float numbers and calculates the product
Here we are using data type double for numbers so that you can enter integer as well as floating point numbers.

In order to read the input provided by user, we first create the object of Scanner by passing System.in as parameter.
Then we are using nextInt() method of Scanner class to read the integer.
 */

import java.util.Scanner;

public class ReadAndProductTwoNumbers2 {
    public static void main(String[] args) {

        /* This reads the input provided by user
         * using keyboard
         */
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter first number: ");

        // This method reads the number provided using keyboard
        double num1 = scan.nextDouble();

        System.out.print("Enter second number: ");
        double num2 = scan.nextDouble();

        // Closing Scanner after the use
        scan.close();

        // Calculating product of two numbers
        double product = num1*num2;

        // Displaying the multiplication result
        System.out.println("Output: "+product);
    }
}
