package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/09/java-program-to-calculate-power-of-a-number/


Program to calculate power of a number
-using while loop

Here number is the base and p is the power (exponent).
So we are calculating the result of number^p
Increment (++) and decrement (—) operators in Java programming let you easily add 1 to, or subtract 1 from, a variable

++i si i++
They both increment the number. ++i is equivalent to i = i + 1 . ...
Both increment the number, but
++i increments the number before the current expression is evaluted, whereas
i++ increments the number after the expression is evaluated.

--i si i--
They both decrement the number. --i is equivalent to i = i - 1 . ...
Both decrement the number, but
--i decrements the number before the current expression is evaluted, whereas
i-- decrements the number after the expression is evaluated.
 */

public class PowerOfANumber2 {
    public static void main(String[] args) {

        int number = 5, p = 2;
        long result = 1;

        int i=p;
        while (i != 0)
        {
            result *= number;
            --i;
        }
        System.out.println(number+"^"+p+" = "+result);


        //varianta mea
        /*
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number: ");

        int num = scan.nextInt();
        System.out.print("Enter the precision: ");

        int prec = scan.nextInt();

        long result = 1;
        int i=prec;
        while (i != 0)
        {
            result *= num;
            --i;
            System.out.println("Variabila i=prec are acum valoarea: "+i);
        }
        System.out.println("Calculul este: "+num+"^"+prec+" = "+result);
        */

    }
}
