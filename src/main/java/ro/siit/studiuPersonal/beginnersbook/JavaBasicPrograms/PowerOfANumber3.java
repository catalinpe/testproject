package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/09/java-program-to-calculate-power-of-a-number/


Program to calculate power of a number
-using pow() functionp

Here number is the base and p is the power (exponent).
So we are calculating the result of number^p
 */

public class PowerOfANumber3 {
    public static void main(String[] args) {
        int number = 10, p = 3;
        double result = Math.pow(number, p);
        System.out.println(number+"^"+p+" = "+result);

        //varianta mea
        /*Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number: ");
        int num = scan.nextInt();

        System.out.print("Enter the precision: ");
        int prec = scan.nextInt();

        scan.close();

        double result1 = Math.pow(num, prec);
        System.out.println("Rezultatul este: "+num+"^"+prec+" = "+result1);
        */
    }
}