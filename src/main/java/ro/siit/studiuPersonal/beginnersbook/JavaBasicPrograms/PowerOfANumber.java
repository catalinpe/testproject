package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/09/java-program-to-calculate-power-of-a-number/

Program to calculate power of a number using for loop
In this program we are calculating the power of a given number
-using for loop.

Here number is the base and p is the power (exponent).
So we are calculating the result of number^p
 */

public class PowerOfANumber {
    public static void main(String[] args) {
        //Here number is the base and p is the exponent
        int number = 2, p = 5;
        long result = 1;

        //Copying the exponent value to the loop counter
        int i = p;
        for (;i != 0; --i)
        {
            result *= number;
        }

        //Displaying the output
        System.out.println(number+"^"+p+" = "+result);
    }
}
