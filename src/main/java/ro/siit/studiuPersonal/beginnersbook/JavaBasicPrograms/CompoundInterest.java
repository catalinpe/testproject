package ro.siit.studiuPersonal.beginnersbook.JavaBasicPrograms;

/*
https://beginnersbook.com/2019/07/java-program-to-calculate-compound-interest/

Java Program to Calculate Compound Interest

Compound interest is calculated using the following formula:

P (1 + R/n) (nt) - P
Here:
P is principal amount.
R is the annual interest rate.
t is the time the money is invested or borrowed for.
n is the number of times that interest is compounded per unit t,
for example if interest is compounded monthly and t is in years then the value of n would be 12.
If interest is compounded quarterly and t is in years then the value of n would be 4.

Let’s say
an amount of $2,000 is deposited into a bank account
as a fixed deposit at an annual interest rate of 8%,
compounded monthly,

the compound interest after 5 years would be:

P = 2000.
R = 8/100 = 0.08 (decimal).
n = 12.
t = 5.

Compound Interest = 2000 (1 + 0.08 / 12) (12 * 5) – 2000 = $979.69

So, the compound interest after 5 years is $979.69.

Formula:
P (1 + R/n) (nt) - P
 */

public class CompoundInterest {

    public void calculate(int p, int t, double r, int n) {
        double amount = p * Math.pow(1 + (r / n), n * t);
        double cinterest = amount - p;
        System.out.println("Compound Interest after " + t + " years: "+cinterest);
        System.out.println("Amount after " + t + " years: "+amount);
    }
    public static void main(String args[]) {
        CompoundInterest obj = new CompoundInterest();
        obj.calculate(2000, 5, .08, 12);
    }


    //varianta mea
    /*
    public void calculate(int p, int t, double r, int n) {
        double amount = p * Math.pow(1 + (r / n), n * t);
        double cinterest = amount - p;

        System.out.println("Compound Interest after " + t + " years: "+cinterest);
        System.out.println("Amount after " + t + " years: "+amount);
    }

    public static void main(String args[]) {
        CompoundInterest obj = new CompoundInterest();

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the principal amount of USD: ");

        int num1 = scan.nextInt();
        System.out.print("Enter the time (years) of investment: ");

        int num2 = scan.nextInt();
        System.out.print("Enter the interest rate: ");

        Double num3 = scan.nextDouble();
        System.out.print("Enter the nr of times (months): ");

        int num4 = scan.nextInt();

        obj.calculate(num1, num2, num3, num4);
        }

     */
}
