package ro.siit.curs3;

public class SumProduct {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]); /*daca nu pun aceasta functie nu va putea face conversia de la sir la nr intreg*/
        int b = Integer.parseInt(args[1]);
        System.out.println("Numarul 1 este : "+a); /* a este adus din linia de comanda din Edit Configurations de sub SumProduct*/
        /*Ctrl+d duplica linia*/
        System.out.println("Numarul 2 este : "+b); /* b este adus din linia de comanda*/
        int sum = a + b;
        int prod = a * b;
        System.out.println("Suma este : "+sum);
        System.out.println("Produsul este : "+prod);

        if (a < b) {
            System.out.println("Numarul : "+ a + " < Numarul : " + b);
        }
        else {
            if (a == b) {
                System.out.println("The numbers are equal");
            }
            else {
                    System.out.println(a + " > " + b);
            }
        }
    }

}
