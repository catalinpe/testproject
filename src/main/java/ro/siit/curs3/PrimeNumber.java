package ro.siit.curs3;

public class PrimeNumber {
    public static void main(String[] args) {
        /* verificam daca un numar este prim */
        final int PRIME_TESTED = 1842325;

        boolean isPrime = true; /* boolean este un stegulet; presupunem ca primul numar este prim */
        for (int i = 2; i < PRIME_TESTED; i++) { /* am pornit de la 2 deoarece fiecare numar se imparte la 1 */
            if (PRIME_TESTED % i == 0) {
                isPrime = false;
                System.out.println("Found divisor "+i);
            }
        }
        if (isPrime) {
            System.out.println("Number este prime");
        }
        else {
            System.out.println("Number is not prime");
        }
    }
}
