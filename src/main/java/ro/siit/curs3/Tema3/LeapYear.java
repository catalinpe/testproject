package ro.siit.curs3.Tema3;

/*
	Automation - Seria 2
	Java intro

	3. Display the number of days in month February from a year between 1900-2016 that is read from argument
	Name the class (and java file) LeapYear.

	HINT

	A year is a leap year if:

	-Is divisible by 4 but not by 100
	-Is divisible by 100 but not by 400
*/

/* asta trebuie sa discutam impreuna */
public class LeapYear {
    public static void main(String[] args) {
        //year to leap year or not
        int year = 1900;
        final int MAX_YEAR = 2016;
        System.out.println(year);
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                System.out.println("Year " + year + " is a leap year");
            else
                System.out.println("Year " + year + " is not a leap year");
            System.out.println();
        }
    }


