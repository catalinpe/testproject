package ro.siit.curs3.Tema3;

/*
	Automation - Seria 2
	Java intro

	2. Display all the prime numbers lower than 1 000 000.
	Name the class (and java file) PrimeNumbers
*/

//Aici vreau sa ne uitam impreuna

public class PrimeNumbers
{
    /* Funcție care verifica dacă un număr este sau nu prim */
    static boolean isPrime(int n)
    {
        // Capatul de inceput
        if (n <= 1)
            return false;

        // Verific de la 2 la n-1
        for (int i = 2; i < n; i++)
            if (n % i == 0) /* n % i == 0 adica n impartit la i */
                return false;

        return true;
    }

    // Functie care afiseaza numerele prime
    static void printPrime(int n)
    {
        for (int i = 2; i <= n; i++)
        {
            if (isPrime(i))
                System.out.print(i + " ");
        }
    }

    // Main-ul
    public static void main(String[] args)
    {
        int n = 1000000;
        printPrime(n);
    }
}