package ro.siit.curs3.Tema3;

/*
	Automation - Seria 2
	Java intro

	1. Calculate the sum of the first 100 numbers higher than 0.
	Name the class (and java file) SumOfNumbers
*/

/* bucla for
ok - functioneaza
 */
//public class SumofNumbers{
//
//    public static void main(String[] args) {
//        int numar;
//        int total = 0;
//
//        for(numar= 1; numar<= 100; numar++){ /* increment: numar++ inseamna numar = numar + 1 */
//            total = total + numar;
//        }
//        System.out.println("Suma primelor 100 de numere mai mari decat 0 este: "+total);
//
//    }
//
//}

/* bucla for
cu afisare a sumei la fiecare iteratie
ok - functioneaza
 */
//public class SumofNumbers{
//
//    public static void main(String[] args) {
//        int sum = 0;
//        final int min = 0;
//
//        for(int i= 1; i<= 100; i++){
//            sum += i; /* asignment operator: sum +=i inseamna sum = sum + i */
//            System.out.println("Am ajuns cu numaratoarea la :"+i+" Suma numerelor de la :"+min+" la :"+i+" este egala cu "+sum);
//         }
//    }
//
//}


/* Sume Gauss */
public class SumofNumbers {

    public static void main(String[] args) {
        int i =100;
        int sum = (i)*(i+1)/2;
        System.out.println("Suma primelor 100 de numere mai mari decat 0, calculata conform Sumelor lui Gauss este: "+sum);

    }
}