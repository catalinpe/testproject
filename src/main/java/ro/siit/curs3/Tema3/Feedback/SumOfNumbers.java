package ro.siit.curs3.Tema3.Feedback;

/*
1.Calculate the sum of the first 100 numbers higher than 0.
Name the class (and java file) SumOfNumbers.
 */

public class SumOfNumbers {

    public static int ComputeSum(int n) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int ComputeSumFormula(int n) {
        return n * (n + 1) / 2;
    }
}

