package ro.siit.curs3.Tema3.Feedback;

public class HomeworkSolution {

    public static void main(String[] args) {
        System.out.println(SumOfNumbers.ComputeSum(100));
        System.out.println(SumOfNumbers.ComputeSumFormula(100));
        PrimeNumbers.CheckPrimeNumbers(1000000);
        // All years to be checked
        LeapYear.LeapYearsChecker(1900, 2016);
        // As argument:
        LeapYear.LeapYearsCheck(Integer.parseInt(args[0]));
    }
}
