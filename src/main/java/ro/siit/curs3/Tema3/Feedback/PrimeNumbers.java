package ro.siit.curs3.Tema3.Feedback;

/*
2.Display all the prime numbers lower than 1 000 000.
Name the class (and java file) PrimeNumbers
 */

public class PrimeNumbers {

    private static Boolean isPrime(int n) {
        boolean isPrime = true;
        for (int i = 2; i < n / 2; i++) {
            if (n % i == 0) {
                isPrime = false;
                //System.out.println("Found divizor " + i);
            }
        }
        return isPrime;
    }

    public static void CheckPrimeNumbers(int maxNUmber) {
        for (int i = 2; i < maxNUmber; i++) {
            if (isPrime(i)) {
                System.out.println("Found prime number: " + i);
            }
        }
    }
}

