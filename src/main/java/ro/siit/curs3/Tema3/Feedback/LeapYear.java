package ro.siit.curs3.Tema3.Feedback;

/*
3. Display the number of days in month February from a year between 1900-2016 that is read from argument
HINT
A year is a leap year if:
-Is divisible by 4 but not by 100
-Is divisible by 100 but not by 400
Name the class (and java file) LeapYear.
 */

public class LeapYear {

    private static  Boolean CheckLeapYar(int year) {
        boolean isLeap = false;
        // if the year is divided by 4
        if (year % 4 == 0) {
            // if the year is century
            if (year % 100 == 0) {
                // if year is divided by 400
                // then it is a leap year
                if (year % 400 == 0)
                    isLeap = true;
            }
            // if the year is not century
            else
                isLeap = true;
        }
        return isLeap;
    }

    public static void LeapYearsCheck(int year) {
        if (year < 1900 || year > 2016){
            System.out.println("The year is not in the range of 1900-2016");
        }
        else {
            LeapYearsChecker(year, year);
            // Another solution is :
            if (CheckLeapYar(year)) {
                System.out.println("Year " + year + " has 29 days for February");
            }
            else {
                System.out.println("Year " + year + " has 28 days for February");
            }
        }
    }

    public static void LeapYearsChecker(int startYear, int endYear) {
        int start = startYear;
        int end = endYear;
        // This checks if the startYear is given after endYear, and if i is true switch them between using the aux var
        if (start > end) {
            int aux = start;
            start = end;
            end = aux;
        }
        for (int i = start; i <= end ; i++) {
            if (CheckLeapYar(i)) {
                System.out.println("Year " + i + " has 29 days for February");
            }
            else {
                System.out.println("Year " + i + " has 28 days for February");
            }
        }
    }
}
