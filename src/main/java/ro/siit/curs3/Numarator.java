package ro.siit.curs3;


public class Numarator {
    public static void main(String[] args) {
        /*WHILE*/
        final int MAX_NUM = 25;
        int count = 1;
        while (count < MAX_NUM) {
            System.out.println(count);
            count++;
            System.out.println("Noua valoare, in while, este " + count);
        }

        /* asta este echivalentul lui while de mai sus*/
        count = 1;
        while (true) {
            if (count >= MAX_NUM) {
                break;
            }
            System.out.println(count);
            count++;
        }

        /*Ddo-while*/
        count = 1;
        do {
            System.out.println("Noua valoare, in do-while, este "+count);
            count++;
        }
        while (count < MAX_NUM);

        /*for*/
        for (int c = 1; c < MAX_NUM; c++) {
            /* c=1 - conditia de pornire;
            c < MAX_NUM - conditia de oprire
            c++ - pasul de incrementare*/
            System.out.println("Noua valoare, in for, este "+c);
        }

        /*iterare prin  lista*/
        int[] lista = {1, 5, 7, 78, 12};

        for (int i = 0; i < lista.length; i++) {
            System.out.println("Valorile sunt :"+lista[i]);
        }

        // se recomanda
        for (int val: lista) {
            System.out.println("Valoarea, in forma recomandata, este :"+val);
        }
    }
}
