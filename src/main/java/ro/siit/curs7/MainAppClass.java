package ro.siit.curs7;

import java.sql.SQLOutput;
import java.util.logging.SocketHandler;

public class MainAppClass {
    public static void main(String[] args) throws Exception {
//        throwMyException();
//        System.out.println("Un mesaj");
        try
        {
            System.out.println(circleAria(-5));
        }
        catch (IllegalArgumentException | ArithmeticException e) { //multiple exceptions in the same catch
            System.out.println(e.getMessage());
        }
        catch (IndexOutOfBoundsException e) {
            System.out.println("index out of bound");
        }
        catch (Exception e) {
            System.out.println("Other exceptions");
        }
        finally {
            System.out.println("This block will always run !!!");
        }
//        System.out.println("ceva");
        try {
            int age = Integer.parseInt(args[0]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("There are no arguments !!");
        }
        try {
            System.out.println(circleAria(-5));
        } catch (Exception e) {
            System.out.println(e.getMessage());;
        }
        System.out.println("final !!!");

        try {
            newThrowException();
        }
        catch (MyCustomException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void throwMyException () {
        throw new IllegalArgumentException("Aici am aruncat exceptia pentru ca ...");
    }

    private static double circleAria (double radius) throws Exception {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be positive value, your value is "+radius);
        }
        return Math.PI*radius*radius;
    }

    private static void newThrowException () throws MyCustomException {
        throw new MyCustomException("This is my custom exception!!!", -200);
    }
}
