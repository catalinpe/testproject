package ro.siit.curs7.Tema7;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;

public class FunctionsCalculatorTest<expected> {

    static FunctionsCalculator c;

    @BeforeClass
    public static void beforeTest() {
        c = new FunctionsCalculator();

    }

    //in Clasa
    //SUM
    @Test
    public void TestSum01() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(2,7,"+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void TestSum02() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(2164,-15646,"+"));
        Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
    }

    @Test
    public void TestSum03() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(1000,0,"+"));
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test
    public void TestDif01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
    }

    @Test
    public void TestProduct01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
    }

    //  @Test
    @Test(expected = IllegalArgumentException.class) //ii spunem testului ca este ok sa dea exceptie
    public void TestUnsupp01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(121, c.compute(11, 11, "X"), 0);
    }

    @Test
    public void TestDiv01() {
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test
    public void TestDiv02() {
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    //----   urmatoarele 2 metode au acelasi rezultat insa cea corecta este a doua pentru tratarea exceptiilor

    //1
    //This is the corect way to test if an exception is expected to be raised
    // @Test
    @Test(expected = IllegalArgumentException.class) //punem asa ca stim sigur ca o sa iasa eroare
    public void TestDiv03() {
        Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
    }

//    //2
//    //This is just to catch exception and the test will be passed eitherway, please to not use this example !!
//    //@Test
//    public void TestDiv04() {
//        try {
//            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
//        }
//        catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//    }

    //---

    @Test
    public void TestSqrt01() {
        Assert.assertEquals(1.4142, c.compute(2, 2, "SQRT"), 0.001);
    }

    //--------------------------------------------------------------------------------
    //TEMA
    //SUM
    @Test
    public void TestSum1() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(1000,0,"+"));
        //Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
        Assert.assertNotEquals(2, c.compute(1, 1,"+"), 0);
    }

    //DIF
    @Test
    public void TestDif1() {
        //Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
        Assert.assertNotEquals(18, c.compute(27,9,"-"), 0.1);
    }

    //PROD
    @Test
    public void TestProd1() {
        //Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
        Assert.assertNotEquals(132, c.compute(12,11,"*"), 0);
    }

    //DIV
    @Test(expected = AssertionError.class)
    public void TestDiv1() {
        //Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
        Assert.assertNotEquals(3, c.compute(10,3,"/"), 0.1);
    }

    //SQRT
    @Test
    public void TestSqrt1() {
        //Assert.assertEquals(1.4142, c.compute(2, 2, "SQRT"), 0.1);
        Assert.assertNotEquals(3.123,c.compute(3.123,2,"SQRT"),0);
    }

    //POW
    @Test
    public void TestPow1() {
        Assert.assertEquals(9, c.compute(3, 2, "POW"), 0);
        Assert.assertNotEquals(9,c.compute(3,2,"POW"),1);
        Assert.assertTrue("Good result",c.compute(9,2,"+") < c.compute(9,2,"POW"));
    }

    //Problema 1
    //exceptand assertNotEquals si assertTrue nu m-am descurcat
    //unde gasesc alte librarii pentru assert care sa se aplice la "double"
}