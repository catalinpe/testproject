package ro.siit.curs5;

public class PersonC5 {
    private String name;
    private String adress;
    private String cnp;
    private int age;
    private String gender;

    public PersonC5(String name, String adress, String cnp, int age, String gender) {
        this.name = name;
        this.adress = adress;
        this.cnp = cnp;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    @Override
    public String toString() {
        return "PersonC5{" +
                "name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", cnp='" + cnp + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }
}
