import org.junit.*;

public class MyFirstTests {

    @BeforeClass //se ruleaza inainte de toate testele
    //trebuie sa fie static, este al clasei si nu al obiectului
    public static void beforeClass () {
        System.out.println("--> This runs before all tests in class");
        System.out.println();
    }

    @Before
    public void beforeTest () {
        System.out.println("- This runs before each test");
    }

    @Test //adnotare de test
    //aceasta notare a permis ca aceasta metoda sa fie rulabila
    //JUnit tine loc de main
    public void test01 () {
        System.out.println("This is my first test !!!");
    }

    @Test
    public void test02 () {
        System.out.println("Second test !!!");
    }

    @Ignore //se foloseste cand mai scriem ceva, cand nu este gata, ceva temporar
    //la testele pe care nu vrei sa le rulez le pun @Ignore
    public void ingnoreTest () {
        System.out.println("This is ignored");
        String a = "abc";
        String b = "abc"; //a si b sun obiecte diferite, cu aceeasi valoare

        String c = "cde";
        String d = c; //c, d sunt referinta --> acelasi obiect cu valoarea respectiva
    }

    @After
    public void afterTest () {
        System.out.println("- This runs after test");
        System.out.println();
    }

    @AfterClass
    public static void afterClass () {
        System.out.println("--> This runs after all tests in class");
    }
}
