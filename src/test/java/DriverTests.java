import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import jdk.nashorn.internal.runtime.regexp.joni.exception.InternalException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class DriverTests {

    WebDriver driver;

    @Test
    public void myFirstDriverTest() {
        //vreau sa setez chromedriver (webdriver.chrome.driver)
        // care se afla in calea .../chromedriver.exe
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        //cream un chromedriver
        //va cauta proprietatea de mai sus ca sa stie ce driver foloseste
        WebDriver driver = new ChromeDriver();

        //mergem pe o pagina de net
        //se pune in partea de before
        // driver-ul este browser-ul
        driver.get("http://www.google.com");

        //defin un webelement
        WebElement searchInput = driver.findElement(By.name("q"));
        //searchInput.sendKeys("koala");
        //dam un Enter
        //searchInput.sendKeys(Keys.ENTER);

        //sau si cu Enter cu tot
        searchInput.sendKeys("koala", Keys.ENTER);

        //informatiile se iau din pagina controlata
        //am luat Copy/Copy XPath pe elementele Koala din pagina cu rezultatele cautarii Koala
        //*[@id="rso"]/div[1]/div[2]/div[1]/a/h3/span
        //*[@id="rso"]/div[2]/div[2]/div[1]/a/h3/span
        //*[@id="rso"]/div[3]/div/div[1]/a/h3/span

        //am generalizat div-urile cu *
        ////*[@id="rso"]/div[*]/div[2]/div[1]/a/h3/span

        //nu este de incredere xpath
        //List<WebElement> titles = driver.findElements(By.xpath("//*[@id=\"rso\"]/div[*]/div/div[1]/a/h3/span\n"));

        //am luat cu Copy/Copy Selector pe elementele Koala din pagina cu rezultatele cautarii Koala
        //#rso > div:nth-child(1) > div.tF2Cxc > div.yuRUbf > a > h3
        //#rso > div:nth-child(2) > div.tF2Cxc > div.yuRUbf > a > h3
        //generalizam
        //#rso > div > div.tF2Cxc > div.yuRUbf > a > h3
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());//size ne da cate elemente am gasit

        //inchidem browser-ul
        //se pune in partea de after
        //driver.quit(); //inchide tot browser-ul (toate tab-page-urile) (se pune la finalul testului)
        //driver.close(); //inchide fereastra curenta
    }

    //cum se porneste un HtmlUnit driver: un browser generic
    @Test
    public void htmlUnitTest() {
        WebDriver driver = new HtmlUnitDriver(); //frate cu ChromeDriver
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    //cum se porneste un Firefox driver
    @Test
    public void firefoxDriverTest() {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        //instantiem driver-ul
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    //cum se porneste un Edge driver
    @Test
    public void edgeDriverTest() {
        System.setProperty("webdriver.edge.driver", "src/test/resources/drivers/msedgedriver.exe");

        //instantiem driver-ul
        WebDriver driver = new EdgeDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());
        driver.quit();
    }

    //cum se porneste un Internet Explorer driver
    //ESTE DE EVITAT
    @Test
    public void ieDriverTest() {
        System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");

        //instantiem driver-ul
        WebDriver driver = new InternetExplorerDriver();
        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());
        driver.quit(); //asta nu functioneaza pe InternetExplorer
    }

    @Test
    public void driverManagerTest() {
        //vede ce versiune de Chrome este instalat si isi instaleaza versiunea de drivermanager de care are nevoie
        //pentru Chrome
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); //this shall be moved to Before
        driver = new ChromeDriver(); //this shall be moved to Before

        //pentru Firefox
        //WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
        //WebDriver driver = new FirefoxDriver();

        //pentru Edge
        //WebDriverManager.getInstance(DriverManagerType.EDGE).setup();
        //WebDriver driver = new EdgeDriver();

        driver.get("http://www.google.com");
        WebElement searchInput = driver.findElement(By.name("q"));
        searchInput.sendKeys("koala", Keys.ENTER);

        System.out.println(driver.getTitle());
        List<WebElement> titles = driver.findElements(By.cssSelector("#rso > div > div > div.yuRUbf > a > h3"));
        System.out.println("numarul de rezultate este: " + titles.size());
        driver.quit(); //this shall be moved to After
    }

    @Test
    public void lazyButtonTest() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); //this shall be moved to Before
        driver = new ChromeDriver(); //this shall be moved to Before
        driver.get("http://86.121.249.149:4999/stubs/lazy.html");

        //browser-ul sa astepte max 15 secunde pentru fiecare element din pagina
        //wait implicit, la nivel de driver. nu este neaparat recomandat
        //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        //cu Ctrl + / se comenteaza cod
//        for (int i = 0; i < 5; i++) {//dau click de 5 ori
//            WebElement lazyButton = driver.findElement(By.id("lazy-button")); //identifica obiectul (butonul) in pagina
//            lazyButton.click();//dau click pe buton
//            //Thread.sleep(15000); //!!!! nu se recomanda aceasta solutie
//        }

        //wait explicit
        //best practice
        //doar pentru elementele care au probleme cu incarcarea
        WebDriverWait wait = new WebDriverWait(driver, 15);
        for (int i = 0; i < 5; i++) {//dau click de 5 ori
            WebElement lazyButton = wait.until(
                    ExpectedConditions.presenceOfElementLocated(By.id("lazy-buton"))
            );
            lazyButton.click();

        }

        //driver.quit();

    }

    @DataProvider(name = "logindp")
    public Iterator<Object[]> loginDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //username, password, username error message, password error message, general error message
        //CAZUL INVALID
        //1. nu completez username si nici password
        dp.add(new String[] {"", "", "Please enter your username", "Please enter your password", ""});
        //2. completez username
        dp.add(new String[] {"aaaa", "", "", "Please enter your password", ""});
        //3. completez password
        dp.add(new String[] {"", "aaaa", "Please enter your username", "", ""});
        //4. completez eronat username si password
        dp.add(new String[] {"aaa", "aaa", "", "", "Invalid username or password!"});

        //CAZUL VALID
        //dp.add(new String[] {"dingo", "dingopassword", "", "",""});

        return dp.iterator();
    }

    @Test(dataProvider = "logindp")
    //valorile din paranteza vin din DataProvider
    public void negativeLoginTest(String username, String password, String userErrMsg, String passwordErrMsg, String generalErrMsg) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); //this shall be moved to Before
        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html");

        //identificam campurile in pagina (3: username, password, buton)
        WebElement usernameInput = driver.findElement(By.id("input-login-username"));
        WebElement passwordInput = driver.findElement(By.id("input-login-password"));
        WebElement submitButton = driver.findElement(By.id("login-submit"));

        //adaugam text in casute (campuri)
        usernameInput.clear(); //goleste daca era ceva inainte
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);

        //actionam butonul
        submitButton.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[2]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"login_form\"]/div[3]/div/div[2]"));
        WebElement err3 = driver.findElement(By.id("login-error"));

//        System.out.println(err1.getText());
//        System.out.println(err2.getText());
//        System.out.println(err3.getText());

        //facem validare
        //validarea testului se face pe baza assert-ului
        Assert.assertEquals(err1.getText(), userErrMsg);
        Assert.assertEquals(err2.getText(), passwordErrMsg);
        Assert.assertEquals(err3.getText(), generalErrMsg);

        driver.close();
    }
}
