import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.siit.curs7.Calculator;


//varianta I
// cu Calculator c definit la nivelul fiecarei metode
/*
public class CalculatorTest {

    @Test
    public void testSum01 () {
        Calculator c = new Calculator();
//        System.out.println(c.compute(2,7,"+"));
        Assert.assertEquals(9,c.compute(2,7,"+"),0);
    }

    @Test
    public void testSum02 () {
        Calculator c = new Calculator();
//        System.out.println(c.compute(2164,-15646,"+"));
        Assert.assertEquals(-13482,c.compute(2164,-15646,"+"),0);
    }

    @Test
    public void testSum03 () {
        Calculator c = new Calculator();
//        System.out.println(c.compute(1000,0,"+"));
        Assert.assertEquals(1000,c.compute(1000,0,"+"),0);
    }

    @Test
    public void testDif01 () {
        Calculator c = new Calculator();
        Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
    }
}

 */

//II
//cu Calculator c definit sus
public class CalculatorTest {

    static Calculator c;

    @BeforeClass
    public static void beforeTest() {
        c = new Calculator();

    }

    @Test
    public void TestSum01() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(2,7,"+"));
        Assert.assertEquals(9, c.compute(2, 7, "+"), 0);
    }

    @Test
    public void TestSum02() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(2164,-15646,"+"));
        Assert.assertEquals(-13482, c.compute(2164, -15646, "+"), 0);
    }

    @Test
    public void TestSum03() {
//        Calculator c = new Calculator();
//        System.out.println(c.compute(1000,0,"+"));
        Assert.assertEquals(1000, c.compute(1000, 0, "+"), 0);
    }

    @Test
    public void TestDif01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(900, c.compute(1000, 100, "-"), 0);
    }


    @Test
    public void TestProduct01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(121, c.compute(11, 11, "*"), 0);
    }

    //  @Test
    @Test(expected = IllegalArgumentException.class) //ii spunem testului ca este ok sa dea exceptie
    public void TestUnsupp01() {
//        Calculator c = new Calculator();
        Assert.assertEquals(121, c.compute(11, 11, "X"), 0);
    }

    @Test
    public void TestDiv01() {
        Assert.assertEquals(2, c.compute(10, 5, "/"), 0);
    }

    @Test
    public void TestDiv02() {
        Assert.assertEquals(3.33, c.compute(10, 3, "/"), 0.01);
    }

    //----   urmatoarele 2 metode au acelasi rezultat insa cea corecta este a doua pentru tratarea exceptiilor

    //1
    //This is the corect way to test if an exception is expected to be raised
    // @Test
    @Test (expected = IllegalArgumentException.class) //punem asa ca stim sigur ca o sa iasa eroare
    public void TestDiv03() {
        Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
    }

//    //2
//    //This is just to catch exception and the test will be passed eitherway, please to not use this example !!
//    //@Test
//    public void TestDiv04() {
//        try {
//            Assert.assertEquals(0, c.compute(10, 0, "/"), 0);
//        }
//        catch (IllegalArgumentException e) {
//            System.out.println(e.getMessage());
//        }
//    }

    //---

    @Test
    public void TestSqrt01() {
        Assert.assertEquals(1.4142, c.compute(2, 2, "SQRT"), 0.001);
    }

}